% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [has_ances, ances_index] = get_has_ancestor(current_index, indizes_list)
%% get_has_ancestor
%       search through the list of indizes if a current index has an
%       ancestor in the wavelet tree that is contained in the node set
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           16.12.2019
%   last Revision:  20.02.2020
%
%   Input:  current_index   ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%                                 stage >=0
%                                 0 <= subband <= (2^stage - 1)
%
%           indizes_list    ...  (M x 1) cell array with (nDim x 2) integers
%                                inlcuding all indizes of the tree
%
%   Output: has_ances       ... [bool] returns true if current_index has an
%                               ancestor in the specified tree
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get parameters
nIndizes = length(indizes_list);

% set default
has_ances = false;
ances_index = 0;

% trace member (children) back trough all parent stages (parent)
for n=1:nIndizes
    
    % compare only indexes with a smaller stage at least in one dimension 
    if all( indizes_list{n}(:,1) <= current_index(:,1) ) && any( indizes_list{n}(:,1) < current_index(:,1) )
        
        % calculate traceback of the current_index knot
        traceback = floor (current_index(:,2)./(2.^(current_index(:,1)-indizes_list{n}(:,1))) );
        
        % compare trace-back with all indizes
        if all(traceback==indizes_list{n}(:,2))
            has_ances = true;
            ances_index = n;
            return
        end
    end
end


end