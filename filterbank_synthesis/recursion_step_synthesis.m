% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function result = recursion_step_synthesis(current_index, fil, mem,ind_reduced_list)
%% recursion_step_synthesis
%       main function for synthese filter bank with cascaded 2 channel filter banks
%       build the tree recursively, gets called by recursion_start_synthesis
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           17.12.2019
%   last Revision:  20.02.2020
%
%   Input:  current_index...  (nDim x 2)
%                                 dim 1   | stage  ,  subband|
%                                 dim 2   | stage  ,  subband|
%                                 dim 3   | stage  ,  subband|
%           fil    ...  filter coefficients 1x2 cell
%           mem       ...  change the order of low- and high-band according to
%                             weickert [integer]
%                             +1: Low not yet analytic
%                             +2: High not yet analytic
%                             -3: High analytic
%                             +3: Low analytic
%           ind_reduced_list...  (array) [int] list with relevant indizes
%                                (sub set of the indizes_list)
%
%   Output: result ...  output signal [M x N] with N=samples, M=nSignals
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global coefs_all;

% indizes list
coef_list = coefs_all(:,2);

%% get properties of current index
% check if current_index is an end knot (leaf of the tree)
% next dimension or stopping condition
[has_descendant, dim, ind_reduced_list,ind_curr] = get_has_descendant(current_index,coef_list,ind_reduced_list);
in_list_curr = ind_curr~=0;

if has_descendant
    
    %% if multiple dimensions are possible, check completeness of indizes
    dims_allowed = [];
    if length(dim)>1
        for n=1:length(dim)
            temp_index = current_index;
            temp_index(dim(n),:) = [current_index(dim(n),1)+1, 2*current_index(dim(n),2)];
            lp_in_list = is_in_coef_list(temp_index,coef_list);
            temp_index(dim(n),:) = [current_index(dim(n),1)+1, 2*current_index(dim(n),2)+1];
            bp_in_list = is_in_coef_list(temp_index,coef_list);
            if lp_in_list && bp_in_list
                dims_allowed(end+1) = dim(n);
            end
        end
        dim = dims_allowed(1);
    end
    
    %% lowpass
    % update [current_index, mem] in the next iteration
    next_index = current_index;
    next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)];
    
    if mem(dim)~=1
        next_mem = mem;
        next_mem(dim) = 3;
    else
        next_mem = mem;
        next_mem(dim) = 1;
    end
    
    % start next recursion to get lowpass signal
    lp_coef = recursion_step_synthesis(next_index, fil, next_mem, ind_reduced_list);
    
    
    %% bandpass
    % update [current_index, mem] in the next iteration
    next_index = current_index;
    next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)+1];
    
    if (mem(dim)~=2) && (current_index(dim,1)>0)
        next_mem = mem;
        next_mem(dim) = -3;
    else
        next_mem = mem;
        next_mem(dim) = 2;
    end
    
    % start next recursion to get bandpass signal
    bp_coef = recursion_step_synthesis(next_index, fil, next_mem, ind_reduced_list);
    
    %% combine lowpass and bandpass signals
    result = prepare_filtering_synthesis(lp_coef, bp_coef,current_index, fil, mem,dim);
else
    if in_list_curr
        result = coefs_all{ind_curr,1};
    else
        % get resulting matrix size
        nSize = size(coefs_all{1,1});
        stages = coefs_all{1,2}(:,1).';
        if length(stages) < length(nSize)
            stages = [stages, zeros(1,length(nSize)-length(stages))];
        end
        sizeMatrix = nSize.*(2.^(stages));
        result = zeros(sizeMatrix);
    end
end
    

end

