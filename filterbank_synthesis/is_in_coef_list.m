% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [in_list,index] = is_in_coef_list(current_index, indizes_list)
%% is_in_coef_list: search through the list of indizes if a current index is contained
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           07.11.2019
%   last Revision:  23.11.2019
%
%   Input:  current_index   ...     (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband| 
%                                   stage >=1
%                                   subband \in [0,2^stage-1]
%           indizes_list    ...     (M x 1) cell array with
%                                   all indizes of the tree
%
%   Output: in_list         ...     [bool] returns true if current_index 
%                                   is in the list
%           index           ...     [int] index of the element
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set default
in_list=false;
index = 0;

% compare current index with all indizes
for n = 1:length(indizes_list)
    if isequal(current_index,indizes_list{n})
        in_list = true;
        index = n;
        return
    end
end


end