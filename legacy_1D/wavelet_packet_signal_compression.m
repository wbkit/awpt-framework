% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [signals_compressed,coefficients] = wavelet_packet_signal_compression(signals, K ,a_fil,s_fil)
%% wavelet_packet_signal_compression:  
%  calculates the signals, after a data compression. The algorithm is based
%  on a best basis search using [1]. After signal decomposition, the
%  coefficients are sorted and the smallest values are removed. The signals
%  are then reconstructed from the compressed signals
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           22.11.2019
%   last Revision:  02.12.2019
%
%   Input:  
%       signals            ...  [nSignal, N] input signals with
%                                   nSignals: number of signals
%                                   N: samples
%       K                  ...  Compression Ratio
%       a_fil              ...  (1x4) (cell array) analysis filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%       s_fil              ...  (1x4) (cell array) synthesis filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%
%   Output: 
%       signals_compressed ...  [nSignals x N] compressed reconstructed signals
%                                  
%       coefficients       ...  {nCoefficients x 3} resulting data
%                               structure
%
%
% See also [1] Coifman, R.R., Wickerhausen, M.V., Entropy-Based Algorithms
% for Best Basis Selection, In: IEEE Transactions on Information Theory,
% 38(2):713-718, 1992
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% signal adapted basis search
% read signal parameters and get tree depth
signal_energy = sum(signals*signals',2);
N = size(signals,2);
fil_len = max([a_fil{3}{1}{3},a_fil{3}{2}{3},a_fil{4}{1}{3},a_fil{4}{2}{3}]);
maxDepth = floor(log2(N/fil_len));

% create full frame decomposition specifications
split_param = set_split_parameters([],'full_frame',maxDepth,[],@hCostEntropy,signal_energy,true);

% decomposition
[coefficients] = wavelet_packet_decomposition(signals, a_fil,split_param);

% find best basis
[coef_list] = find_best_basis(coefficients);

%% signal decomposition with optimal basis
split_param = set_split_parameters(coef_list,'preset_tree');
[coefficients] = wavelet_packet_decomposition(signals, a_fil,split_param);

%% compression
coefficients = coefficient_compression(coefficients,K);

%% reconstruction
signals_compressed  = wavelet_packet_synthesis(coefficients, s_fil);

end







%% local cost function for best basis search
%% local cost functions
function [cost_pos,cost_neg] = hCostEntropy(coefs,signal_energy)
%%hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
nBatches = length(coefs.re);
[nSignals, ~] = size(coefs.re{nBatches});
batch_cost_pos = zeros(nSignals,nBatches);
batch_cost_neg = zeros(nSignals,nBatches);

% do for every batch
for idx=1:nBatches
    
    % positive frequency
    % get absolute values
    coefs_abs = (abs(coefs.re{idx} + 1i*coefs.im{idx}).^2)./signal_energy;
    % remove zeros and NaN
    coefs_abs(coefs_abs==0) = NaN;
    % calculate entropy of signal energy normed coefficients
    batch_cost_pos(:,idx) = -sum((coefs_abs).*log(coefs_abs),2,'omitnan');
    
    % negative frequency
    % get absolute values
    coefs_abs = (abs(coefs.re{idx} - 1i*coefs.im{idx}).^2)./signal_energy;
    % remove zeros and NaN
    coefs_abs(coefs_abs==0) = NaN;
    % calculate entropy of signal energy normed coefficients
    batch_cost_neg(:,idx) = -sum((coefs_abs).*log(coefs_abs),2,'omitnan');
end
% the costs of different batches are averaged
cost_pos = mean(batch_cost_pos(:));
cost_neg = mean(batch_cost_neg(:));
end