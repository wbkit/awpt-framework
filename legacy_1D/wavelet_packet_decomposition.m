% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef] = wavelet_packet_decomposition(signal, fil,split_param)
%% wavelet_packet_transformation:  calculates the WPT of the Signal with an example Tree
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           11.10.2019
%   last Revision:  09.12.2019
%
%   Input:  signal    ...  (array or matrix) signal
%           fil       ...  (1x4) (cell array) filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%           split_param ... struct with parameters which stages and
%                           subbands to use
%
%   Output: coef      ...  (N x 3 cell matrix) coefficients of the WPT with
%                           meta data {complex coefficients, [stage, subband], costs}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reset warning state
lastwarn('')

%% condition input signal type
if iscell(signal)
    sig = signal;
else
    sig = {signal};
end

%% condition input filter structure
if length(fil)==1
    fil=fil{1};
end

%% condition split_params
if ~strcmp(split_param.Mode,'automatic')
    if size(split_param.coef_indizes{1},1)>1
        for n=1:length(split_param.coef_indizes)
            split_param.coef_indizes{n} = split_param.coef_indizes{n}(1,:);
        end
    end
end

%% zero padding to get signal length as power of 2
[P,L] = size(sig{1});
if rem(log2(L),1)
    for n=1:length(sig)
        sig{n} = [sig{n}, zeros(P,2^ceil(log2(L))-L)];
    end
end

%% Analytische WPT
[coef] = first_stage_split(sig, fil, split_param);

% If input is not cell unpack Output
if ~iscell(signal)
    coef = coef{1};
end

end
