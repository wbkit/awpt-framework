% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [has_parent] = get_has_parent(current_index, indizes_list)
%% get_has_parent: search through the list of indizes if a current index has a parent in the wavelet tree that is contained in the coefficient list
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           07.11.2019
%   last Revision:  25.11.2019
%
%   Input:  current_index   ...     [int,int] 
%                                   current_index(1): stage >=1
%                                   current_index(2): subband \in [0,2^stage-1]
%           indizes_list    ...     (M x 1) cell array with [int,int] with
%                                   all indizes of the tree
%
%   Output: has_parent       ...  [bool] returns true if current_index has a
%                                parent in the specified tree
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set default
has_parent=false;

% trace member (children) back trough all parent stages (parent)
while(current_index(1)>1)
    
    % trace back current index one generation
    current_index = [current_index(1)-1,floor(current_index(2)/2)];
    
    % compare trace-back with all indizes
    for n = 1:length(indizes_list)
        if isequal(current_index,indizes_list{n})
            has_parent = true;
            return
        end
    end
end


end