% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function result = first_stage_synthesis(coef, fil)
%% first_stage_synthesis   main function for synthese filter bank with cascaded 2 channel filter banks
%                                      build the tree recursively
%                                      initial point for recursive algorithm
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  11.12.2019 (Daniel Schwaer)
%
%   Input:  coef   ...  signal coefficients to use for reconstruction
%                       (nCoefs x 3) cell array
%           fil    ...  filter coefficients 1x2 cell 
%
%   Output: result ...  output signal [nSignals x N]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global coefs_all;
coefs_all = coef;

%% read parameters
% indizes list
coef_list = coef(:,2);

[in_list_lp,ind_lp] = is_in_coef_list([1,0],coef_list);
[in_list_hp,ind_hp] = is_in_coef_list([1,1],coef_list);



%% Tief- bzw Bandpass weiter aufspalten
% Tiefpass
if ~in_list_lp
    lp_coef_re = next_stage_synthesis(1,0, fil(3:4), 1);
else
    % get real-tree parts, by combining positive and negative
    % frequencys
    if size(coefs_all,2) == 3
        lp_coef_re = sqrt(2)*real(coefs_all{ind_lp,1});
    else
        lp_coef_re = 1/sqrt(2)*(coefs_all{ind_lp,1} + coefs_all{ind_lp,4});
    end
end

% Bandpass
if ~in_list_hp
    hp_coef_re = next_stage_synthesis(1,1, fil(3:4), 2);
else
    % get real-tree parts, by combining positive and negative
    % frequencys
    if size(coefs_all,2) == 3
        hp_coef_re = sqrt(2)*real(coefs_all{ind_hp,1});
    else
        hp_coef_re = 1/sqrt(2)*(coefs_all{ind_hp,1} + coefs_all{ind_hp,4});
    end
end

result = stage_combine(lp_coef_re, hp_coef_re, fil, 1);

% check if analytical or real wavelet packets are used
if ~isequal(fil{1}{1},fil{1}{2}) % if (lowpass real) == (lowpass imag) then its real 
    % recombine real and imaginary tree (with scaling by sqrt(2), because signal
    % energy has to be kept constant)
    result = sqrt(2)*result;
end

end






