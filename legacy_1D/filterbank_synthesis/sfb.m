% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function y = sfb(lo_sig, hi_sig, fil)
%% SFB  phase adapted 2 channel synthese filter bank
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  18.12.2019 (Matthias Baechle
%
%   Input:  lo_sig ...  low subband signal [1xN]
%           hi_sig ...  high subband signal [1xN]
%           fil    ...  filter coefficients 1x2 cell 
%
%   Output: y      ...  output signal [1 x 2N]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% upsample signals
lo_sig = upsample(lo_sig.',2).';
hi_sig = upsample(hi_sig.',2).';

% extract filters
f0b = fil{1}{1};
f0a = fil{1}{2};
EL0 = fil{1}{3};
EM0 = fil{1}{4};

f1b = fil{2}{1};
f1a = fil{2}{2};
EL1 = fil{2}{3};
EM1 = fil{2}{4};

% delay between lp and hp has to be even 
M = 2;
EM0 = M*round(EM0/M);
EM1 = M*round(EM1/M);

%% distinguish FIR <-> IIR
if f0a==1
    % use FIR method
    
    % use zero padding
    [P_lo,L_lo] = size(lo_sig);
    [P_hi,L_hi] = size(hi_sig);
    lo_sig_zp = [lo_sig, zeros(P_lo, EL0)];
    hi_sig_zp = [hi_sig, zeros(P_hi, EL1)];
    
    % filter lowpass signal
    temp = filter(f0b,f0a,lo_sig_zp, [], 2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-L_lo)=temp(:,1:Lt-L_lo)+temp(:,L_lo+1:end);
    sig_lp = temp(:,1:L_lo);
    
    % filter highpass signal
    temp = filter(f1b,f1a,hi_sig_zp, [],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-L_hi)=temp(:,1:Lt-L_hi)+temp(:,L_hi+1:end);
    sig_bp = temp(:,1:L_hi);
    
    % shift signals back with each individual shift
    sig_lp = circshift(sig_lp, -length(f0b)+1+EM0,2);
    sig_bp = circshift(sig_bp, -length(f0b)+1+EM1,2);

    % recombine both signal parts
    y = sig_lp+sig_bp;
else
    % use IIR method
    
    % use zero padding
    MEM = round((EM0+EM1)/2);
    [P_lo,L_lo] = size(lo_sig);
    [P_hi,L_hi] = size(hi_sig);
    lo_sig_zp = [lo_sig, zeros(P_lo, EL0+MEM)];
    hi_sig_zp = [hi_sig, zeros(P_hi, EL1+MEM)];
    
    % filter lowpass signal
    temp = filter(f0b,f0a,lo_sig_zp, [], 2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-L_lo)=temp(:,1:Lt-L_lo)+temp(:,L_lo+1:end);
    sig_lp = temp(:,1:L_lo);
    
    
    % filter highpass signal
    temp = filter(f1b,f1a,hi_sig_zp, [],2);
    [~,Lt] = size(temp);
    temp(:,1:Lt-L_hi)=temp(:,1:Lt-L_hi)+temp(:,L_hi+1:end);
    sig_bp = temp(:,1:L_hi);
    
    % shift signals back with each individual shift
    sig_lp = circshift(sig_lp, -length(f0b)+1+EM0,2);
    sig_bp = circshift(sig_bp, -length(f0b)+1+EM1,2);
    
    % recombine both signal parts
    y = sig_lp+sig_bp;
end






