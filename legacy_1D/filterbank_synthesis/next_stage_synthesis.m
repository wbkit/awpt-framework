% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function result = next_stage_synthesis(stage, subband, fil, mem)
%% next_stage_synthesis   main function for synthese filter bank with cascaded 2 channel filter banks
%                                      build the tree recursively, gets
%                                      called by complex_init_mfb_synthesis
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  11.12.2019 (Daniel Schwaer)
%
%   Input:  tree   ...  tree structure to use for reconstruction [cell]
%           coef   ...  signal coefficients to use for reconstruction [cell array]
%           fil    ...  filter coefficients 1x2 cell
%           mem       ...  change the order of low- and high-band according to
%                             weickert [integer]
%                             +1: Low not yet analytic
%                             +2: High not yet analytic
%                             -3: High analytic
%                             +3: Low analytic
%
%   Output: result ...  output signal [M x N] with N=samples, M=nSignals
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global coefs_all;

% indizes list
coef_list = coefs_all(:,2);

[in_list_lp,ind_lp] = is_in_coef_list([stage+1,2*subband],coef_list);
[in_list_hp,ind_hp] = is_in_coef_list([stage+1,2*subband+1],coef_list);

% Tiefpass
if ~in_list_lp
    if mem~=1
        next_mem = 3;
    else
        next_mem = 1;
    end
    lp_coef_re = next_stage_synthesis(stage+1,2*subband, fil, next_mem);
else
    % get real-tree parts, by combining positive and negative
    % frequencys
    if size(coefs_all,2) == 3
        lp_coef_re = sqrt(2)*real(coefs_all{ind_lp,1});
    else
        lp_coef_re = 1/sqrt(2)*(coefs_all{ind_lp,1} + coefs_all{ind_lp,4});
    end
end

% Bandpass
if  ~in_list_hp
    if mem~=2
        next_mem = -3;
    else
        next_mem = 2;
    end
    hp_coef_re = next_stage_synthesis(stage+1,2*subband+1, fil, next_mem);
else
    % get real-tree parts, by combining positive and negative
    % frequencys
    if size(coefs_all,2) == 3
        hp_coef_re = sqrt(2)*real(coefs_all{ind_hp,1});
    else
        hp_coef_re = 1/sqrt(2)*(coefs_all{ind_hp,1} + coefs_all{ind_hp,4});
    end
end
    
result = stage_combine(lp_coef_re, hp_coef_re, fil, mem);

end

