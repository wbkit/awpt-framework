% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef,f_vec] = calc_base_response_1D(a_fil,stage,subband,time_step,N,f_vec,fs)
%% calc_base_response_1D
%       calculates the response of AWP to a harmonics excitation
%       with specified frequencies
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           11.11.2019
%   last Revision:  20.02.2020
%
%   Input:  a_fil       ...  (1x4) (cell array) analysis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           stage      ...  [int] scale in the wavelet tree >0
%           subband    ...  [int] in [0,2^stage-1] specifies branch
%           time_step  ...  [int] time step of the coeficient in the
%                           subband \in [0,N*2^(-stage)-1]
%           N          ...  number of samples of base function
%                           will be zeropadded to get a multiple of 2^stage
%           f_vec      ...  (1 x K) vector of frequencies
%           fs         ...  sampling frequency
%
%   Output: coef      ...   resulting (1 x K) complex response of the wavelet packet
%                          
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% prepare input signals and catch exceeding limits
nCoeffs_min = max([a_fil{1}{1}{3},...
                   a_fil{1}{2}{3},...
                   a_fil{2}{1}{3},...
                   a_fil{2}{2}{3}...
                   ]);
% condition test signal length
N_min = 2^(ceil(log2(nCoeffs_min)))*2^(stage-1);
if N<N_min
    N = N_min;
else
    N = 2^(ceil(log2(N)));
end

% condition time_step
if time_step < 0, time_step = 0; end
if time_step > N/(2^stage)-1, time_step = N/(2^stage)-1; end

% condition subband
if subband < 0, subband = 0; end
if subband > 2^stage-1, subband = 2^stage-1; end

% create time vector
dt = 1/fs;
t = 0:dt:(N-1)*dt;

% preallocate memory
coef = zeros(1,length(f_vec));
coef_list = cell(stage+1,1);

% define wavelet tree
stage_temp = stage;
subband_temp = subband;
coef_list{1} = [stage,subband];

for k=2:stage+1
    if mod(subband_temp,2) == 0
        coef_list{k} = [stage_temp,subband_temp+1];
    else
        coef_list{k} = [stage_temp,subband_temp-1];
    end
    stage_temp= stage_temp-1;
    subband_temp = floor(subband_temp/2);
end

split_param = set_split_parameters(coef_list);

%% sweep through all frequencies
for n = 1:length(f_vec)
    f_i = f_vec(n);
    
    % create testsignal
    sig = exp(2i*pi*f_i*t);
    
    % perform wavelet decomposition
    [coefs] = wavelet_packet_decomposition(sig, a_fil,split_param);
    
    % search correct stage and subband
    coef_list = coefs(:,2);
    
    [~,index] = is_in_coef_list([stage,subband], coef_list);
    
    % read coefficients and save to vector
    coef(n) = coefs{index,1}(time_step+1);
end

end