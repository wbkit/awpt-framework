% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [split_param] = set_split_parameters(coef_indizes,Mode,maxDepth,stopping_threshold,hCostFun,CostParameters,CreateCostTree)
%% set_filter_parameters set the filter parameters according to the 
%                        specified parameters and set default if nothing 
%                        is specified
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           10.10.2019
%   last Revision:  02.12.2019 (Daniel Schwaer)
%
%   Input:      coef_indizes    ...     (M x 1) cell array list of coef
%                                       indizes [stage,subband]
%                                       overwritten by following parameters
%               Mode            ...     [char array] tree creation method
%                                       'preset_tree'
%                                       'automatic'
%                                       'wavelet_tree'
%                                       'full_tree'
%                                       'full_frame'
%               maxDepth        ...     [int] maximum stages of tree
%               stopping_threshold ...  [double] threshold for stopping criteria
%               hCostFun        ...     handle to custom cost function
%               CostParameters  ...     additional data passed to cost
%                                       function
%
%   Output:     split_params    ...     struct with parameters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialize default parameter
split_param.coef_indizes = coef_indizes;
split_param.Mode = 'preset_tree';
split_param.maxDepth = 6;
split_param.stopping_threshold = [];
split_param.hCostFun = @hCostEnergy;
split_param.CostParameters = [];
split_param.CreateCostTree = false;

%% set parameters
if nargin > 1, split_param.Mode = Mode; end
if nargin > 2, split_param.maxDepth = maxDepth; end
if nargin > 3, split_param.stopping_threshold = stopping_threshold; end
if nargin > 4, split_param.hCostFun = hCostFun; end
if isempty(split_param.hCostFun), split_param.hCostFun=@hCostEnergy; end
if nargin > 5, split_param.CostParameters = CostParameters; end
if nargin > 6, split_param.CreateCostTree = CreateCostTree; end
if nargin > 7, warning('Too many input arguments (max=7). I am going to ignore additional arguments.'); end

% create maximum depth missing warning
if ~strcmp(split_param.Mode, 'preset_tree') && ~exist('maxDepth', 'var')
    warning('Maximum depth not set. Fallback do maximum depth = 6')
end

%% check if combination of parameters is valid.
% If not -> Mode = 'full_tree', maxDepth = 6
switch split_param.Mode
    case 'preset_tree'
        % catch error of empty coefficient list
        if isempty(coef_indizes)
            warning(['List of Indizes is empty. Fallback to full tree with maximum depth = ',num2str(split_param.maxDepth),'.'])
            split_param  = set_split_parameters([], 'full_tree', split_param.maxDepth);
        else
            split_param.maxDepth = [];
        end
    case 'wavelet_tree'
        % create coefficient list for wavelet tree
        split_param.coef_indizes = cell(split_param.maxDepth+1,1);
        for m=1:split_param.maxDepth
            split_param.coef_indizes{m}   = [m,1];
        end
        split_param.coef_indizes{end} = [m,0];
    case 'full_tree'
        % create coefficient list for full tree (bandpass is also splitted)
        split_param.coef_indizes = cell(2^(split_param.maxDepth),1);
        for m=1:2^(split_param.maxDepth)
            split_param.coef_indizes{m} = [split_param.maxDepth,m-1];
        end
    case 'full_frame'
        % create coefficient list for full tree with all redundant knots
        split_param.coef_indizes = cell(0,1);
        for k=1:split_param.maxDepth
            for n=1:(2^k)
                split_param.coef_indizes{end+1,1} = [k,n-1];
            end
        end
    case 'automatic'
        % create warning if automatic mode is used without eneough parameters
        if nargin <= 3
            warning(['Not enough input parameters for automatic mode. Fallback to full tree with maximum depth = ',num2str(split_param.maxDepth),'.']);
            split_param  = set_split_parameters([], 'full_tree', split_param.maxDepth);
        elseif nargin == 4
            warning('No cost function given. Fallback to negativ signal energy as cost function.');
        elseif nargin ==5
            warning('Not enough parameters for cost function. I am going to ignore the handle of the cost function and use signal energy as cost function.');
            split_param.hCostFun = @hCostEnergy;
        end
    otherwise
        warning(['Unknown decomposition mode. Allowed parameters are ''automatic'',''wavelet_tree'',''preset_tree'',''full_tree''. Fallback to full tree with maximum depth = ',num2str(split_param.maxDepth),'.']);
        split_param = set_split_parameters([],'full_tree', split_param.maxDepth);
end
end





%% default cost function if no cost function is specified
function [cost_pos,cost_neg] = hCostEnergy(coefs,parameters)
%% hCostEnergy   Calculates the negativ coefficient energy as costs.
%                Multiple batches are considered by averaging
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % get number of batches
    nBatches = length(coefs.re);
    batch_cost_pos = zeros(1,nBatches);
    batch_cost_neg = zeros(1,nBatches);
    % calulcate the coefficient energy for all batches
    for idx=1:nBatches
        batch_cost_pos(idx) = -mean(sum(abs(1/sqrt(2)*(coefs.re{idx} + 1i*coefs.im{idx})).^2,2),1);
        batch_cost_neg(idx) = -mean(sum(abs(1/sqrt(2)*(coefs.re{idx} - 1i*coefs.im{idx})).^2,2),1);
    end
    
    % average over all batches
    cost_pos = mean(batch_cost_pos);
    cost_neg = mean(batch_cost_neg);
end
