% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_FindBestBasis.m:  calculate a full cost tree using the analytic wavelet packets
%                         uses the full frame (all stages and subbands are
%                         considered)
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.11.2019
%   last Revision:  20.02.2020
%
% This function is the test script for the function find_best_basis.m
% The results are representated by a compressed signal vs the original signal
% 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear;
clc;

%% Defines
nSignals = 2;
N = 8192;
Fs = 50e6;
Ts = 1/Fs;
t = 0:Ts:(N-1)*Ts;

% create test signals
signal(1,:) = sin(2*pi*(700e3 + 20e6/(2*8192*2e-8)*t).*t);
signal(2,:) = sin(2*pi*(-1000e3 + 20e6/(2*8192*2e-8)*t).*t);
signal_energy = sum(abs(signal).^2,2);

% filter design
[a_fil,s_fil] = set_filters('db',10,2,2,'hwlet',20,1,6, 'min_phase');

% split parameter
split_param = set_split_parameters([],'full_frame',9,[],@hCostEntropy,signal_energy,true);

% decomposition
[coef] = wavelet_packet_decomposition(signal, a_fil,split_param);

% find best basis
[coef_list] = find_best_basis(coef);

% decompose using best basis
split_param = set_split_parameters(coef_list,'preset_tree');
[coef] = wavelet_packet_decomposition(signal, a_fil,split_param);

% compress signal % todo future feature
coef = coefficient_compression(coef,4);

% reconstruction
signals_reconstructed  = wavelet_packet_synthesis(coef, s_fil);

% plot signals vs reconstructed signals
figure;
subplot(2,1,1);
plot(t,signal(1,:),t,signals_reconstructed(1,:),'.');
title('Signal 1');
subplot(2,1,2);
plot(t,signal(1,:),t,signals_reconstructed(1,:),'.');
title('Signal 2');

% show reconstruction error
ReconstructionError = sum(abs(signal(1,:) - signals_reconstructed(1,:)).^2)/sum(abs(signal(1,:)).^2)
ReconstructionError = sum(abs(signal(2,:) - signals_reconstructed(2,:)).^2)/sum(abs(signal(2,:)).^2)



%% local cost function for best basis search
%% local cost functions
function [cost_pos,cost_neg] = hCostEntropy(coefs,signal_energy)
%%hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
nBatches = length(coefs.re);
[nSignals, ~] = size(coefs.re{nBatches});
batch_cost_pos = zeros(nSignals,nBatches);
batch_cost_neg = zeros(nSignals,nBatches);

% do for every batch
for idx=1:nBatches
    
    % positive frequency
    % get absolute values
    coefs_abs = abs(coefs.re{idx} + 1i*coefs.im{idx}).^2;
    % remove zeros and NaN
    coefs_abs(isnan(coefs_abs)) = [];
    coefs_abs(coefs_abs==0) = [];
    % calculate entropy of signal energy normed coefficients
    batch_cost_pos(:,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),2);
    
    % negative frequency
    % get absolute values
    coefs_abs = abs(coefs.re{idx} - 1i*coefs.im{idx}).^2;
    % remove zeros and NaN
    coefs_abs(isnan(coefs_abs)) = [];
    coefs_abs(coefs_abs==0) = [];
    % calculate entropy of signal energy normed coefficients
    batch_cost_neg(:,idx) = -sum((coefs_abs./signal_energy).*log(coefs_abs./signal_energy),2);
end
% the costs of different batches are averaged
cost_pos = mean(batch_cost_pos(:));
cost_neg = mean(batch_cost_neg(:));
end



