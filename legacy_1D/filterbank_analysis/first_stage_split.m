% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coefs] = first_stage_split(signal, fil, split_param)
%% first_stage_split:  berechnet Stufe 1 Filter der analytische Transformationen
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           11.10.2019
%   last Revision:  13.12.2019
%
%   Input:  signal      ...  (1x2) cell array of (array or matrix) signal [M x N]
%           fil         ...  (1 x 4 cell array) filter coefficients 
%                            {First Stage TP, First stage BP, Next Stage TP, Next Stage BP}
%           split_param ...  struct with parameters which stages and
%                            subbands to use
%                           
%   Output: coefs       ...  (N x 3 cell matrix) coefficients of the WPT with
%                           meta data {complex coefficients, [stage, subband], costs}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% define results variables as global to prevent memory overflow due to
% redundancy
nBatches = length(signal);
global result;
result = cell(1,nBatches);

[~,L] = size(signal{1});

% signal split into real and imaginary part
if ~isequal(fil{1}{1},fil{1}{2})
    % analytical wavelet packets
    in_coef.re = cellfun(@times,signal,num2cell(1/sqrt(2)*ones(1,nBatches)),'un',0);
    in_coef.im = cellfun(@times,signal,num2cell(1/sqrt(2)*ones(1,nBatches)),'un',0);
else
    % real wavelet packets
    in_coef.re = signal;
    in_coef.im = [];
end


%% next split, filter arrangement according to first stage
[lowCoef, highCoef] = stage_split(in_coef, fil(1:2),1);


%% decide if the children should be further split
[split,save_split,cost_low,cost_high] = decide_split(fil, L, split_param, lowCoef, highCoef,0,0);


% Tiefpass
if split(1)
    
    % save if frame specified
    if save_split(1)
        save_knot(lowCoef, cost_low, nBatches, 1, 0, split_param.CreateCostTree)
    end
    
    % begin recursive call
    next_stage_split(lowCoef,1,0, fil(3:4), 1, split_param);
    
else % stop recursive call
    save_knot(lowCoef, cost_low, nBatches, 1, 0, split_param.CreateCostTree)
end

% Bandpass
if split(2) 
    
    % save if frame specified
    if save_split(1)
        save_knot(highCoef, cost_high, nBatches, 1, 1, split_param.CreateCostTree)
    end
    
    % begin recursive call
    next_stage_split(highCoef,1,1, fil(3:4), 2, split_param);
    
else % stop recursive call
    save_knot(highCoef, cost_high, nBatches, 1, 1, split_param.CreateCostTree)
end

coefs=result;

end
