% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef_list,cost] = find_best_basis(coef)
%% find_best_basis:  Starts at the branches of the tree and traces the best 
%                    knots back to the roots using the principle of dynamic
%                    programming.
%                    The cost of the parent knot is calculated as
%                    min(cost_parent, sum(cost_children)).
%                    For this algorithm an entropy based additiv cost function
%                    should be used.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.11.2019
%   last Revision:  13.12.2019
%
%   Input:  coef        ...  (cell array) Filterkoeffizienen
%
%   Output: coef_list   ...  (1 x M cell array) of [stage, subband]
%
% This function is an implementation of the algorithmus of Coifman and
% Wickerhausen [1]
%
% See also [1] Coifman, R.R., Wickerhausen, M.V., Entropy-Based Algorithms
% for Best Basis Selection, In: IEEE Transactions on Information Theory,
% 38(2):713-718, 1992
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% define global variable
global temp_list;

% get coef structure
if iscell(coef)
    if iscell(coef{1,1})
        nBatches = size(coef,2);
        calcInBatches = true;
    else
        nBatches = 1;
        coef = {coef};
        calcInBatches = false;
    end
else
    coef_list = [];
    cost = [];
    warning('Wrong input data type.');
    return
end

coef_list = cell(1,nBatches);

for k=1:nBatches
    
    % preallocate and clean temp_list
    temp_list = cell(0,1);
    
    % start recursion
    cost = get_child_cost(coef{k},0,0);
    
    % typecast to cell
    coef_list_temp = cell(0,1);
    
    % clean redundant coefficients
    for n=1:length(temp_list)
        
        % check if coefficient has parent in the list
        has_parent = get_has_parent(temp_list{n},temp_list);
        
        % if coefficient has no parent, keep it
        if ~has_parent
            coef_list_temp(end+1,1) = temp_list(n);
        end
    end
    
    % save result to batches
    coef_list{1,k} = coef_list_temp;
end

% create output structure depending on the input structure
if ~calcInBatches
    coef_list = coef_list{1};
end

end







%% local recursive function
function cost = get_child_cost(coef,stage,subband)
%% get_child_cost recursive function to calculate the children costs
%                 stops if there are no children anymore

% read global variables
global temp_list;


% check tree structure if knot exists and has children
child_exist = get_has_child([stage,subband],coef(:,2));
[in_list,current_index] = is_in_coef_list([stage,subband],coef(:,2));

% get costs of parent (this knot [stage,subband])
if in_list
    if size(coef,2)== 5
        cost_parent = coef{current_index,3} + coef{current_index,5};
    else
        cost_parent = coef{current_index,3};
    end
else
    cost_parent = inf;
    
    % catch missing knots
    if ~child_exist
        warning('No full reconstruction possible. Missing tree branches.');
        cost = inf;
        return
    end
end

if child_exist
    % next recursive call
    
    % get children costs of bandpass and lowpass
    cost_child_lp = get_child_cost(coef,stage+1,2*subband);
    cost_child_bp = get_child_cost(coef,stage+1,2*subband+1);
    
    % get the minimum cost, the children are summed before comparison
    [cost,ind] = min([cost_parent,cost_child_bp+cost_child_lp]);
    
    % if the parents are chosen, add this knot to the list
    if ind==1
        temp_list(end+1,1) = {[stage,subband]};
    end
else
    % stop recursive call
    cost = cost_parent;
    temp_list(end+1,1) = {[stage,subband]};
end
    
end