% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [tpSignal, bpSignal] = stage_split(sig, fil,mem)
%% stage_split:  spaltet das Eingangssignal nach Band und Tiefpass auf
%   
%   Author:         Daniel Schwär
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           24.04.2019
%   last Revision:  03.12.2019 (Matthias Baechle)
%
%   Input:  sig          ...  input coefficients
%                               struct.re  = cell array
%                               struct.im  = cell array
%           fil          ...  (cell array) Filterkoeffizienten
%                                     {Tiefpass, Bandpass}
%           mem          ...   [1,2,3,-3] current state of 
%                              filter arrangement
%
%   Output: tpSignal      ...  (struct) resultierende Tiefpasssignal
%                                           .re: Realteil
%                                           .im: Imaginärteil
%           bpSignal      ...  (struct) resultierende Bandpasssignal
%                                           .re: Realteil
%                                           .im: Imaginärteil
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract filter pairs
fil_re{1} = fil{1}{1};
fil_re{2} = fil{2}{1};
fil_im{1} = fil{1}{2};
fil_im{2} = fil{2}{2};

% check if analytical or real wavelet packets are used
if isequal(fil_im,fil_re)
    % calc real wavelet packets, if same filters are used
    for idx=1:length(sig.re)
        
        switch mem
            case 1  % BP->BP, TP->TP
                [tpSignal.re{idx},bpSignal.re{idx}]= afb(sig.re{idx}, fil_re);
            case 2  % TP->BP, BP->TP
                [bpSignal.re{idx},tpSignal.re{idx}] = afb(sig.re{idx}, fil_re);
            case 3  % BP->BP, TP->TP
                [tpSignal.re{idx},bpSignal.re{idx}]= afb(sig.re{idx}, fil_re);
            case -3 % TP->BP, BP->TP
                [bpSignal.re{idx},tpSignal.re{idx}] = afb(sig.re{idx}, fil_re);
        end
        % set imaginary parts to zero
        tpSignal.im{idx} = zeros(size(tpSignal.re{idx}));
        bpSignal.im{idx} = zeros(size(tpSignal.re{idx}));
    end
else
    % calc analytical wavelet packets
    for idx=1:length(sig.re)
        
        switch mem
            case 1  % BP->BP, TP->TP , Re->Real_tree , Im->Im_tree
                [tpSignal.re{idx},bpSignal.re{idx}]= afb(sig.re{idx}, fil_re);
                [tpSignal.im{idx},bpSignal.im{idx}] = afb(sig.im{idx}, fil_im);
            case 2  % TP->BP, BP->TP , Im->Real_tree , Re->Im _tree
                [bpSignal.re{idx},tpSignal.re{idx}] = afb(sig.re{idx}, fil_im);
                [bpSignal.im{idx},tpSignal.im{idx}] = afb(sig.im{idx}, fil_re);
            case 3  % BP->BP, TP->TP , Re->Real_tree , Re->Im_tree
                [tpSignal.re{idx},bpSignal.re{idx}]= afb(sig.re{idx}, fil_re);
                [tpSignal.im{idx},bpSignal.im{idx}] = afb(sig.im{idx}, fil_re);
            case -3 % TP->BP, BP->TP , Re->Real_tree , Re->Im_tree
                [bpSignal.re{idx},tpSignal.re{idx}] = afb(sig.re{idx}, fil_re);
                [bpSignal.im{idx},tpSignal.im{idx}] = afb(sig.im{idx}, fil_re);
        end
        
    end
end

end