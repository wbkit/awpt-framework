% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function next_stage_split(in_coef,stage,subband, fil, mem, split_param)
%% next_stage_split:  calculates signal coefficients after second stage
%                              controls the recursive split
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           08.10.2019
%   last Revision:  13.12.2019
%
%   Input:  in_coef   ...  (array or matrix) input coefficients
%           fil       ...  (1 x 2)(cell array) filter coefficients
%                                  {Tiefpass, Bandpass}
%           stage     ...  stage of the input coefficients
%           subband   ...  subband of the input coefficients
%           mem       ...  change the order of low- and high-band according to
%                             weickert [integer]
%                             +1: Low not yet analytic
%                             +2: High not yet analytic
%                             -3: High analytic
%                             +3: Low analytic
%           split_param ... struct with parameters which stages and
%                           subbands to use
%
%   Output: result    ...  the results are written in a global variable by
%                          adding a new entry in the cell-list
%                          {complex coefficients, [stage, subband], costs}
%
%
%   !!!This is a recursive function!!!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nBatches = length(in_coef.re);
sigLength = size(in_coef.re{1}, 2);

%% next split, filter arrangement is defined by mem
[lowCoef, highCoef] = stage_split(in_coef, fil,mem);


%% decide if the children should be further split
[split,save_split,cost_low,cost_high] = decide_split(fil, sigLength, split_param, lowCoef, highCoef,stage,subband);

% lowpass
if split(1)
    
    % save if frame specified
    if save_split(1)
        save_knot(lowCoef, cost_low, nBatches, stage+1, 2*subband, split_param.CreateCostTree)
    end
    
    % calculate filter arrangement for next stage
    if mem~=1
        next_mem = 3; 
    else
        next_mem = 1; 
    end
    
    % continue recursive call
    next_stage_split(lowCoef,stage+1,2*subband, fil, next_mem, split_param);
    
else % stop recursive call
    save_knot(lowCoef, cost_low, nBatches, stage+1, 2*subband, split_param.CreateCostTree)
end

% bandpass
if split(2)
    
    % save if frame specified
    if save_split(2)
        save_knot(highCoef, cost_high, nBatches, stage+1, 2*subband+1, split_param.CreateCostTree)
    end
    
    % calculate filter arrangement for next stage
    if mem~=2
        next_mem = -3; 
    else
        next_mem = 2;
    end
    
    % continue recursive call
    next_stage_split(highCoef,stage+1,2*subband+1, fil, next_mem, split_param);
    
else % stop recursive call
    save_knot(highCoef, cost_high, nBatches, stage+1, 2*subband+1, split_param.CreateCostTree)
end


end
