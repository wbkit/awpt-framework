% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [an_fil, syn_fil] = get_example_filter(N)
%% example_filter:  returns example filter coefficents
%
%   Author:         Daniel Schwaer
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           18.11.2019
%   last Revision:  20.02.2020
%
%   Input:  N         ...   Number of example filter (1-6)
%                               real WPT:
%                                   1: Daubechie 10
%                                   2: Coiflets 10
%                                   3: Symlets 10
%                                   4: Fejer-Korovkin 10
%                               analytic WPT (FIR):
%                                   5: Daubechie 10, qshift 14
%                                   6: Coflets 10, hwlets 20
%                               analytic WPT (IIR):
%                                   7: Intermediate Daubechies-Butterworth Scaling FIlter, hwlets 20
%
%   Output: a_fil     ...   analysis filters
%                               {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                               {real=1,imag=2}
%                               {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
%           s_fil     ...   synthesis filters
%                               {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                               {real=1,imag=2}
%                               {coeffs_num=1,coeffs_denom=2}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch N
    % real wavelet packets
    case 1
        [an_fil, syn_fil] = get_filters('db',5,0,0);
    case 2
        [an_fil, syn_fil] = get_filters('coif',5,0,0);
    case 3
        [an_fil, syn_fil] = get_filters('sym',5,0,0);
%     case 4
%         [first_fil, sec_fil] = set_filter_parameters('fk',5,0,0);
    % analytic wavelet packets
    case 4
        [an_fil, syn_fil] = get_filters('db',5,0,0,'qshift',14);
    case 5
        [an_fil, syn_fil] = get_filters('sym',5,0,10,'hwlet',20,1,6, 'min_phase');
    % IIR-Filter
    case 6
        [an_fil, syn_fil] = get_filters('idabusfi',1,0,10,'hwlet',20,1,6, 'min_phase');
    otherwise
        error('N is out of range')
end

end