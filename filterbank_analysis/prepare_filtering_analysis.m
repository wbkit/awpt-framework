% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [lpSignal, bpSignal] = prepare_filtering_analysis(sig, current_index, fil,mem,dim, ReIm_pattern)
%% prepare_filtering_analysis:  
%       decides the arrangement of the filters to
%       split the input signal along the specified dimension in its parts
%       lowpass and bandpass
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           05.12.2019
%   last Revision:  20.02.2020
%
%   Input:  sig             ...  cell array (1 x 2^nDims) of input coefficients
%           current_index   ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           fil         ...  (cell array) filter coefficients
%                               {dim 1/2/3}
%                               {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                               {real=1,imag=2}
%                               {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
%
%           mem         ...   (1 x nDims) vector of [1,2,3,-3] current state of 
%                             filter arrangement
%           dim         ...   in which dimension to filter
%           ReIm_pattern ...  (nCrossTerms x nDim) of [0,1]
%                             Arrangement of Real or Imag filters in the
%                             coef cell array
%
%   Output: tpSignal    ...  (struct) resultierende Tiefpasssignal
%                                           .re: Realteil
%                                           .im: Imaginärteil
%           bpSignal    ...  (struct) resultierende Bandpasssignal
%                                           .re: Realteil
%                                           .im: Imaginärteil
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% extract filter pairs depending on the stage
if current_index(dim,1) == 0
    fil = fil{dim}(1:2);
else
    fil = fil{dim}(3:4);
end

% preallocate memory
lpSignal = cell(1,length(sig));
bpSignal = cell(1,length(sig));

for idx=1:length(sig{1,1})
    
    switch mem(dim)
        case 1  % BP->BP, TP->TP , Re->Real_tree , Im->Im_tree
            
            % extract filter pairs (arrangement depending on mem
            fil_re{1} = fil{1}{1};
            fil_re{2} = fil{2}{1};
            fil_im{1} = fil{1}{2};
            fil_im{2} = fil{2}{2};
            
            for n=1:length(sig)
                if ReIm_pattern(n,dim)==0
                    [lpSignal{1,n}{idx},bpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_re,dim);
                else
                    [lpSignal{1,n}{idx},bpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_im,dim);
                end
            end
            
            
        case 2  % TP->BP, BP->TP , Im->Real_tree , Re->Im _tree
            
            % extract filter pairs (arrangement depending on mem
            fil_re{1} = fil{1}{2};
            fil_re{2} = fil{2}{2};
            fil_im{1} = fil{1}{1};
            fil_im{2} = fil{2}{1};
            
            for n=1:length(sig)
                if ReIm_pattern(n,dim)==0
                    [bpSignal{1,n}{idx},lpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_re,dim);
                else
                    [bpSignal{1,n}{idx},lpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_im,dim);
                end
            end
            
        case 3  % BP->BP, TP->TP , Re->Real_tree , Re->Im_tree
            
            % extract filter pairs (arrangement depending on mem
            fil_re{1} = fil{1}{1};
            fil_re{2} = fil{2}{1};
            fil_im{1} = fil{1}{1};
            fil_im{2} = fil{2}{1};
            
            for n=1:length(sig)
                if ReIm_pattern(n,dim)==0
                    [lpSignal{1,n}{idx},bpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_re,dim);
                else
                    [lpSignal{1,n}{idx},bpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_im,dim);
                end
            end
            
        case -3 % TP->BP, BP->TP , Re->Real_tree , Re->Im_tree
            
            % extract filter pairs (arrangement depending on mem
            fil_re{1} = fil{1}{1};
            fil_re{2} = fil{2}{1};
            fil_im{1} = fil{1}{1};
            fil_im{2} = fil{2}{1};
            
            for n=1:length(sig)
                if ReIm_pattern(n,dim)==0
                    [bpSignal{1,n}{idx},lpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_re,dim);
                else
                    [bpSignal{1,n}{idx},lpSignal{1,n}{idx}] = filtering_step_analysis(sig{1,n}{idx}, fil_im,dim);
                end
            end
            
    end
    
end


end