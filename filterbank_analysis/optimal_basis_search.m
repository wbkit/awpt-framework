% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef_list,cost] = optimal_basis_search(coef)
%% optimal_basis_search:  Starts at the branches of the tree and traces the best 
%                    knots back to the roots using the principle of dynamic
%                    programming.
%                    The cost of the parent knot is calculated as
%                    min(cost_parent, sum(cost_children)).
%                    For this algorithm an entropy based additiv cost function
%                    should be used.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           23.01.2020
%   last Revision:  20.02.2020
%
%   Input:  coef        ...  (cell array) Filterkoeffizienen
%
%   Output: coef_list   ...  (1 x M cell array) of [stage, subband]
%
% This function is an implementation of the algorithmus of Coifman and
% Wickerhausen [1]
%
% See also [1] Coifman, R.R., Wickerhausen, M.V., Entropy-Based Algorithms
% for Best Basis Selection, In: IEEE Transactions on Information Theory,
% 38(2):713-718, 1992
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% define global variable
global temp_list global_list;

% get coef structure
if iscell(coef)
    if iscell(coef{1,1})
        nBatches = size(coef,2);
        calcInBatches = true;
    else
        nBatches = 1;
        coef = {coef};
        calcInBatches = false;
    end
else
    coef_list = [];
    cost = [];
    warning('Wrong input data type.');
    return
end

coef_list = cell(1,nBatches);

for k=1:nBatches
    
    % preallocate and clean temp_list
    temp_list = cell(0,3);
    global_list = coef{k}(:,2);
    
    % start recursion
    [cost,coef_list{1,k}] = get_child_cost(coef{k}, 0*coef{k}{1,2}, 1:length(coef{k}(:,2)), 'awp');

end

% create output structure depending on the input structure
if ~calcInBatches
    coef_list = coef_list{1};
end

end







%% local recursive function
function [cost,coef_list] = get_child_cost(coef,current_index,ind_reduced_list,mode)
%% get_child_cost recursive function to calculate the children costs
%                 stops if there are no children anymore

% read global variables
global global_list;


% check tree structure if knot exists and has children
[descendant_exist, dim_vec, ind_reduced_list_out, coef_ind] = get_has_descendant(current_index,coef(:,2),ind_reduced_list);
in_list = coef_ind ~= 0 ;

% get costs of parent (this knot [current_index])
if in_list
    if strcmp(mode,'awp')
        cost_parent = coef{coef_ind,3};
    else
        cost_parent = coef{coef_ind,5};
    end
    if isequal(global_list{coef_ind},-1*ones(size(current_index)))
        already_done = true;
    else
        already_done = false;
    end
else
    already_done = false;
    cost_parent = inf;
end

if already_done
    % stop recursive call
    
    cost = inf;
    coef_list = [];
    
elseif descendant_exist
    % next recursive call
    
    % preallocate child cost vector
    child_costs = inf(1,length(dim_vec));
    coefs_dim_lp = cell(1,length(dim_vec));
    coefs_dim_bp = cell(1,length(dim_vec));
    
    for n_dim=1:length(dim_vec)
        
        % get next dimension
        dim = dim_vec(n_dim);
        
        % get children costs of bandpass and lowpass
        % lowpass
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)];
        [cost_child_lp, coefs_dim_lp{n_dim}] = get_child_cost(coef, next_index, ind_reduced_list_out, mode);
        % bandpass
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)+1];
        [cost_child_bp, coefs_dim_bp{n_dim}] = get_child_cost(coef, next_index, ind_reduced_list_out, mode);
        
        % combine costs
        child_costs(n_dim) = cost_child_lp + cost_child_bp;
        
    end
    
    % get the minimum cost, the children are summed before comparison
    [cost,ind] = min([cost_parent,child_costs]);
    
    % if the parents are chosen, add this knot to the list
    if ind==1
        coef_list = {current_index};
    else
        coef_list = [coefs_dim_lp{ind-1},coefs_dim_bp{ind-1}];
    end
    
else
    
    % stop recursive call
    cost = cost_parent;
    if in_list
        coef_list = {current_index};
    else
        coef_list = [];
    end
    
end

% save results of current index
if ~already_done && in_list
    global_list{coef_ind} = -1*ones(size(current_index));
end


end
    