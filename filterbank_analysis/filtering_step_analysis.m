% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [lp_out, bp_out] = filtering_step_analysis(sig, fil,dim)
%% filtering_step_analysis:
%       creates lowpass and bandpass child, by filtering with subsequent
%       downsampling by factor 2
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           04.12.2019
%   last Revision:  20.02.2020
%
%   Input:  sig        ...  input signal [M x N x ...]
%           fil        ...  filter coefficients (1 x 2)(1 x 4) cell
%           dim        ...  in which dimension to filter
%
%   Output: lp_out     ...  low subband output signal  [M x N/2 x ...]
%           bp_out     ...  high subband output signal [M x N/2 x ...]
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% extract filter coefficients, effective filter lengths, and midpoints
% lowpass
lowpass_b      = fil{1}{1};
lowpass_a      = fil{1}{2};
lowpass_length = fil{1}{3};
lowpass_mid    = fil{1}{4};
% bandpass
bandpass_b      = fil{2}{1};
bandpass_a      = fil{2}{2};
bandpass_length = fil{2}{3};
bandpass_mid    = fil{2}{4};

% calculate shift values for convolution
M = 2;
lowpass_mid  = M*round(lowpass_mid/M);
bandpass_mid = M*round(bandpass_mid/M);


%% distinguish FIR <-> IIR
if lowpass_a==1
    % use FIR method

    % shift signals in the specified dimension
    N_vec = size(sig);
    sig_lp= circshift(sig, -lowpass_mid,dim);
    sig_bp = circshift(sig, -bandpass_mid,dim);
    
    % zeropadding in specified dimension
    N = N_vec(dim);
    S.type = '()';
    S.subs = repmat({':'},1,ndims(sig));
    
    % zeropadping lowpass
    S.subs{dim} = N+1:N+lowpass_length;
    sig_lp = subsasgn(sig_lp,S,0);
    % zeropadding bandpass
    S.subs{dim} = N+1:N+bandpass_length;
    sig_bp = subsasgn(sig_bp,S,0);
    
    
    %% filter lowpass
    sig_lp_filtered = filter(lowpass_b,lowpass_a,sig_lp,[],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(sig_lp_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(sig_lp_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end =subsref(sig_lp_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_lp_filtered = subsasgn(sig_lp_filtered,S,sig_beginning + sig_end);
    
    
    %% filter bandpass
    sig_bp_filtered = filter(bandpass_b,bandpass_a,sig_bp,[],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(sig_bp_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(sig_bp_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end =subsref(sig_bp_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_bp_filtered = subsasgn(sig_bp_filtered,S,sig_beginning + sig_end);
    
    
    %% cut signal and downsample
    S.subs{dim} = 1:2:N;
    lp_out = subsref(sig_lp_filtered,S);
    bp_out = subsref(sig_bp_filtered,S);
    
else
    % use IIR method
    
    MEM = round((lowpass_mid+bandpass_mid)/2);
    
    % shift signals in the specified dimension
    N_vec = size(sig);
    sig_lp= circshift(sig, -lowpass_mid,dim);
    sig_bp = circshift(sig, -bandpass_mid,dim);
    
    % zeropadding in specified dimension
    N = N_vec(dim);
    S.type = '()';
    S.subs = repmat({':'},1,ndims(sig));
    
    % zeropadping lowpass
    S.subs{dim} = N+1:N+lowpass_length + MEM;
    sig_lp = subsasgn(sig_lp,S,0);
    % zeropadding bandpass
    S.subs{dim} = N+1:N+bandpass_length + MEM;
    sig_bp = subsasgn(sig_bp,S,0);
    
    
    %% filter lowpass
    sig_lp_filtered = filter(lowpass_b,lowpass_a,sig_lp,[],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(sig_lp_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(sig_lp_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end =subsref(sig_lp_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_lp_filtered = subsasgn(sig_lp_filtered,S,sig_beginning + sig_end);
    
    
    %% filter bandpass
    sig_bp_filtered = filter(bandpass_b,bandpass_a,sig_bp,[],dim);
    
    % get signal lengths in every dimension
    N_zp_vec = size(sig_bp_filtered);
    N_zp = N_zp_vec(dim);
    
    % get signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_beginning = subsref(sig_bp_filtered,S);
    % get signal end
    S.subs{dim} = N+1:N_zp;
    sig_end =subsref(sig_bp_filtered,S);
    
    % assign the sum of signal beginning and signal end to signal beginning
    S.subs{dim} = 1:N_zp-N;
    sig_bp_filtered = subsasgn(sig_bp_filtered,S,sig_beginning + sig_end);
    
    
    %% cut signal and downsample
    S.subs{dim} = 1:2:N;
    lp_out = subsref(sig_lp_filtered,S);
    bp_out = subsref(sig_bp_filtered,S);
end














