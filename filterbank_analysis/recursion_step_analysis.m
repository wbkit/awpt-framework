% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function recursion_step_analysis(in_coef,current_index,fil,mem,split_param)
%% recursion_step_analysis:  
%       calculates signal coefficients after second stage
%       controls the recursive split
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           05.12.2019
%   last Revision:  01.04.2020
%
%   Input:  in_coef   ...  cell array (1 x 2^nDims) of input coefficients
%           current_index ... (nDim x 2)
%                                 dim 1   | stage  ,  subband|
%                                 dim 2   | stage  ,  subband|
%                                 dim 3   | stage  ,  subband|
%           fil       ...  (1 x 2)(cell array) filter coefficients
%                                  {Tiefpass, Bandpass}
%           
%           mem       ...  change the order of low- and high-band according to
%                             weickert [integer]
%                             +1: Low not yet analytic
%                             +2: High not yet analytic
%                             -3: High analytic
%                             +3: Low analytic
%           split_param ... struct with parameters which stages and
%                           subbands to use
%
%   Output: Result is saved in global variable 'result'
%
%
%   !!!This is a recursive function!!!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global result coef_list;
nBatches = length(in_coef{1,1});
nDim = size(coef_list{1},1);

% get imaginary pattern
ReIm_pattern = split_param.ReImPattern;
imag_pattern = 1i.^sum(ReIm_pattern,2);

%% get properties of current index
% next dimension or stopping condition
[has_desc, dim_vec, split_param.ind_reduced_list,coef_ind] = get_has_descendant(current_index,coef_list,split_param.ind_reduced_list);
split = recursion_stop_analysis(in_coef, has_desc, dim_vec, current_index, fil, split_param);

% check if the current index is contained in the list
save_knot = coef_ind~=0;

if ~isempty(split)
    
    %% save current knot, if knot specified in indizes list
    if save_knot
        
        % remove coefficient from list
        coef_list{coef_ind} = -1*ones(nDim,2);
        
        in_coef_WPT = in_coef{1,1};
        in_coef_AWP = cell(1,nBatches);
        
        % create analytical coefficients for cost function
        for idx = 1:nBatches
            in_coef_AWP{idx} = 0;
            for n=1:length(in_coef)
                in_coef_AWP{idx} = in_coef_AWP{idx} + imag_pattern(n)*in_coef{1,n}{idx};
            end
        end
        % calculate coef costs (lower cost is better, can be negative)
        [metaData{1} , metaData{2} ] = split_param.hMetaFun(in_coef_WPT, in_coef_AWP ,split_param.MetaParameters);
        
        if split_param.OnlyMetaData
            % save results for all batches
            for idx=1:nBatches
                result{idx}(end+1,:) = {[] ,...
                    current_index,...
                    metaData{1},...
                    [],...
                    metaData{2}};
            end
        else
            % save results for all batches
            for idx=1:nBatches
                result{idx}(end+1,:) = {in_coef_AWP{idx},...
                    current_index,...
                    metaData{1},...
                    in_coef_WPT{idx},...
                    metaData{2}};
            end
        end
        
    end
    
    for n_dim=1:length(split)
        
        %% further splitting, begin recursion
        dim = split(n_dim);
        [lowCoef, highCoef] = prepare_filtering_analysis(in_coef, current_index, fil,mem, dim, ReIm_pattern);
        
        % lowpass
        % update [current_index, mem] in the next iteration
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)];
        
        if mem(dim)~=1
            next_mem = mem;
            next_mem(dim) = 3;
        else
            next_mem = mem;
            next_mem(dim) = 1;
        end
        
        % split next knot
        recursion_step_analysis(lowCoef,next_index,fil,next_mem,split_param);
        
        
        % bandpass
        % update [current_index, mem] in the next iteration
        next_index = current_index;
        next_index(dim,:) = [current_index(dim,1)+1, 2*current_index(dim,2)+1];
        
        if (mem(dim)~=2) && (current_index(dim,1)>0)
            next_mem = mem;
            next_mem(dim) = -3;
        else
            next_mem = mem;
            next_mem(dim) = 2;
        end
        
        % split next knot
        recursion_step_analysis(highCoef,next_index,fil,next_mem,split_param);
    end
else
    %% stop splitting
    % remove coefficient from list
    if coef_ind>0
        coef_list{coef_ind} = -1*ones(nDim,2);
    end
    
    in_coef_WPT = in_coef{1,1};
    in_coef_AWP = cell(1,nBatches);
    
    % create analytical coefficients for cost function
    for idx = 1:nBatches
        in_coef_AWP{idx} = 0;
        for n=1:length(in_coef)
            in_coef_AWP{idx} = in_coef_AWP{idx} + imag_pattern(n)*in_coef{1,n}{idx};
        end
    end
    % calculate coef costs (lower cost is better, can be negative)
    [metaData{1} , metaData{2} ] = split_param.hMetaFun(in_coef_WPT, in_coef_AWP ,split_param.MetaParameters);
    
    if split_param.OnlyMetaData
        % save results for all batches
        for idx=1:nBatches
            result{idx}(end+1,:) = {[] ,...
                current_index,...
                metaData{1},...
                [],...
                metaData{2}};
        end
    else
        % save results for all batches
        for idx=1:nBatches
            result{idx}(end+1,:) = {in_coef_AWP{idx},...
                current_index,...
                metaData{1},...
                in_coef_WPT{idx},...
                metaData{2}};
        end
    end
    
end


end
