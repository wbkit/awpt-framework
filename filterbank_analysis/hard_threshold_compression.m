% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef] = hard_threshold_compression(coef,K,sort_basis)
%% hard_treshold_compression:
%  Sorts the coefficients and removes the smallest coefficients to reach the
%  specified Compression Ratio. The absolute value is used as a default for
%  the sorting algorithm.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           20.01.2020
%   last Revision:  20.02.2020
%
% Input:
%       coef    ...     structure resulting from
%                       wavelet_packet_decomposition
%       K       ...     compressed coefficients / uncompressed coefficients
%       sort_basis ...  [optional]: 'abs', 'real'
%                       specifies on which basis the coefficients should be
%                       sorted
%
% Output:
%       coef ...        same structure as coef  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set defaul values
if nargin < 3
    sort_basis='abs';
end

% check if input is structured in batches
if iscell(coef{1,1})
    nBatches = size(coef,2);
    calcInBatches = true;
else
    nBatches = 1;
    coef = {coef};
    calcInBatches = false;
end


for n=1:nBatches
    % unpack signal
    coef_temp = coef{n};
    % concatenate coefficients
    coef_sizes = cellfun(@size,coef_temp(:,1),'un',0);
    coef_shaped_analytic = cellfun(@(x) reshape(x,1,[]),coef_temp(:,1),'un',0);
    coef_shaped_real = cellfun(@(x) reshape(x,1,[]),coef_temp(:,4),'un',0);
    coef_lengths = cellfun(@length,coef_shaped_real);
    
    coef_concatenated_analytic = cell2mat(coef_shaped_analytic.');
    coef_concatenated_real = cell2mat(coef_shaped_real.');

    N = length(coef_concatenated_analytic);
    
    % create sorting order by ascending absolute value
    switch sort_basis
        case 'abs'
            [~,sort_ind] = sort(abs(coef_concatenated_analytic),'descend');
        case 'real'
            [~,sort_ind] = sort(abs(real(coef_concatenated_real)),'descend');
    end
    
    [~,rev_sort_ind] = sort(sort_ind);
    
    %% sort data -> cut of smallest elements -> sort back
    % sort
    coef_compressed_real = coef_concatenated_real(sort_ind);
    coef_compressed_analytic = coef_concatenated_analytic(sort_ind); 
    
    % cut
    coef_compressed_real(floor(K*N):end) = 0;
    coef_compressed_analytic(floor(K*N):end) = 0;
    
    % sort back
    coef_compressed_analytic = coef_compressed_analytic(rev_sort_ind);
    coef_compressed_real = coef_compressed_real(rev_sort_ind);
    
    % create cell structure once again
    coef_compressed_analytic = mat2cell(coef_compressed_analytic,1,coef_lengths).';
    coef_compressed_real = mat2cell(coef_compressed_real,1,coef_lengths).';
    
    coef_compressed_analytic = cellfun(@(x,y) reshape(x,y), coef_compressed_analytic, coef_sizes, 'un',0);
    coef_compressed_real = cellfun(@(x,y) reshape(x,y), coef_compressed_real, coef_sizes, 'un',0);
    
    % pack signal
    coef{n}(:,1) = coef_compressed_analytic;
    coef{n}(:,4) = coef_compressed_real;
    
    
end

% create output structure depending on the input structure
if ~calcInBatches
    coef = coef{1};
end

end