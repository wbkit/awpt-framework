% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [split_param] = get_decomposition_structure(coef_indizes,Mode,dim,maxDepth,hMetaFun,MetaParameters,OnlyMetaData)
%% get_decomposition_structure
%       set the filter parameters according to the specified parameters 
%       and set default if nothing is specified
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           04.12.2019
%   last Revision:  27.02.2020
%
%   Input:      coef_indizes    ...     (M x 1) cell array list of coef
%                                       indizes [stage,subband]
%                                       overwritten by following parameters
%               Mode            ...     [char array] tree creation method
%                                       'preset_tree'
%                                       'wavelet_tree'
%                                       'full_tree'
%                                       'full_frame'
%               dim             ...     dimension of the wavelets
%               maxDepth        ...     (1 x nDim) [int] maximum stages of tree
%               hMetaFun        ...     handle to function, which
%                                       calculates meta data corresponding to the current nodes
%                                       data (can be cost functional or
%                                       some feature space)                  
%               MetaParameters  ...     additional data passed to the
%                                       hMetaFun function
%               OnlyMetaData  ...     [bool] defines if only the cost of
%                                       the coefficients should be saved
%
%   Output:     split_params    ...     struct with parameters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% condition list of coefs
if size(coef_indizes,2) > size(coef_indizes,1), coef_indizes = coef_indizes.'; end

%% Initialize default parameter
split_param.coef_indizes = coef_indizes;
split_param.Mode = 'preset_tree';
split_param.dim = 1;
split_param.maxDepth = 3;
split_param.hMetaFun = @hEmpty;
split_param.MetaParameters = [];
split_param.OnlyMetaData = false;

%% set parameters
if nargin > 1, split_param.Mode = Mode; end
if nargin > 2, split_param.dim = dim; end
if nargin > 3
    if length(maxDepth) >= dim
        split_param.maxDepth = maxDepth(1:dim);
    else
        split_param.maxDepth = [maxDepth,repmat(maxDepth,1,dim-length(maxDepth))];
    end
end
if nargin > 4, split_param.hMetaFun = hMetaFun; end
if nargin > 5, split_param.MetaParameters = MetaParameters; end
if nargin > 6, split_param.OnlyMetaData = OnlyMetaData; end
if nargin > 7, warning('AWPT:get_decomposition_structure:TooManyInputArgs','Too many input arguments (max=7). I am going to ignore additional arguments.'); end

% create maximum depth missing warning
if ~strcmp(split_param.Mode, 'preset_tree') && ~exist('maxDepth', 'var')
    warning('AWPT:get_decomposition_structure:MissingMaxDepth','Maximum depth not set. Fallback do maximum depth = 3')
end

%% check if combination of parameters is valid.
% If not -> Mode = 'full_tree', maxDepth = 6
switch split_param.Mode
    case 'preset_tree'
        % catch error of empty coefficient list
        if isempty(coef_indizes)
            warning('AWPT:get_decomposition_structure:MissingIndizes',['List of Indizes is empty. Fallback to full tree with maximum depth = ',num2str(split_param.maxDepth(1)),'.'])
            split_param  = get_decomposition_structure([], 'full_tree', split_param.dim, split_param.maxDepth(1));
        else
            split_param.dim = size(coef_indizes{1},1);
            indizes_mat = cell2mat(coef_indizes);
            split_param.maxDepth = max(reshape(indizes_mat(:,1),split_param.dim,[]),[],2).';
        end
    case 'wavelet_tree'
        % create coefficient list for wavelet tree
        
        % preallocate memory
        subbands = cell(1,split_param.dim);
        split_param.coef_indizes = cell(0,1);
        
        % define subband numbering
        subband = 0:1;
        
        % pack subband numbering to cell
        for k=1:split_param.dim
            subbands{k} = subband;
        end
        
        % create subband meshgrid with arbitrary dimensions
        meshbands = cell(split_param.dim,1);
        [meshbands{:}] = ndgrid(subbands{:});
        meshbands = cellfun(@(x) reshape(x,1,[]), meshbands,'un',0);
        meshbands = cell2mat(meshbands);
        
        % create coefficient list using the subband mesh and the stage
        for k=1:split_param.maxDepth(1)
            for n=2:size(meshbands,2)
                split_param.coef_indizes{end+1,1} = [k*ones(split_param.dim,1),...
                                                   meshbands(:,n)];
            end
        end
        % add last lowpass split to coefficient list
        split_param.coef_indizes{end+1,1} = [split_param.maxDepth(1)*ones(split_param.dim,1),...
                                                   meshbands(:,1)];
        split_param.maxDepth = split_param.maxDepth(1)*ones(1,split_param.dim);
        
    case 'full_tree'
        % create coefficient list for full tree (bandpass is also splitted)
        
        % preallocate memory
        subbands = cell(1,split_param.dim);
        
        % pack subband numbering to cell
        for k=1:split_param.dim
            subbands{k} = 0:2^(split_param.maxDepth(k))-1;
        end
        
        % create subband meshgrid with arbitrary dimensions
        meshbands = cell(split_param.dim,1);
        [meshbands{:}] = ndgrid(subbands{:});
        meshbands = cellfun(@(x) reshape(x,1,[]), meshbands,'un',0);
        meshbands = cell2mat(meshbands);
        
        % create coefficient list using the subband mesh and the maximum stage
        for k=1:size(meshbands,2)
            split_param.coef_indizes{k,1} = [split_param.maxDepth.' , meshbands(:,k)];
        end
            
    case 'full_frame'
        % create coefficient list for full tree with all redundant knots
        
        % preallocate memory
        split_param.coef_indizes = cell(0,1);
        list = cell(split_param.dim,1);
        
        % create index list for every dimension
        for n=1:split_param.dim
            list{n} = cell(0,1);
            for k=1:split_param.maxDepth(n)
                for l=1:(2^k)
                    list{n}{end+1} = [k,l-1];
                end
            end
        end
        
        % calculate full factorial combinations
        dim_ind_lengths = 2.^(split_param.maxDepth+1)-2;
        dim_indizes = fullfact(dim_ind_lengths);
        
        % combine all dimensions in one list
        for k=1:size(dim_indizes,1)
            index = zeros(split_param.dim,2);
            for n=1:split_param.dim
                index(n,:) = list{n}{dim_indizes(k,n)};
            end
            split_param.coef_indizes{end+1,1} = index;
        end
        
    otherwise
        warning('AWPT:get_decomposition_structure:UnknownMode',['Unknown decomposition mode. Allowed parameters are ''wavelet_tree'',''preset_tree'',''full_tree''. Fallback to full tree with maximum depth = ',num2str(split_param.maxDepth),'.']);
        split_param = get_decomposition_structure([],'full_tree',split_param.dim, split_param.maxDepth);
end

end




%% default empty function if no meta data function is necessary
function [cost_awp,cost_real] = hEmpty(coefsWPT,coefsAWP,parameters)
%% hEmpty returns nothing as feature
    cost_awp = [];
    cost_real = [];
end


%% meta function examples 

% % signal energy of the data in the current node
% function [cost_awp,cost_real] = hCostEnergy(coefsWPT,coefsAWP,parameters)
% %% hCostEnergy   Calculates the signal energy as costs.
% %                Multiple batches are considered by averaging
% %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%     % get number of batches
%     nBatches = length(coefsWPT);
%     batch_cost_awp = zeros(1,nBatches);
%     batch_cost_real = zeros(1,nBatches);
%     
%     % calulcate the coefficient energy for all batches
%     for idx=1:nBatches
%         batch_cost_awp(idx) = -sum(abs(coefsAWP{idx}).^2,'all');
%         batch_cost_real(idx) = -sum(abs(coefsWPT{idx}).^2,'all');
%     end
%     
%     % average over all batches
%     cost_awp = mean(batch_cost_awp);
%     cost_real = mean(batch_cost_real);
% end

% % shannon entropy of the data in the current node
% function [cost_awp,cost_real] = hCostEntropy(coefsWPT,coefsAWP,signal_energy)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hCostEntropy  handle for cost function of the coefficients
% %               Entropy based
% %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % preallocate memory
% % get number of batches
% nBatches = length(coefsWPT);
% batch_cost_awp = zeros(1,nBatches);
% batch_cost_real = zeros(1,nBatches);
% 
% % do for every batch
% for idx=1:nBatches
%     
%     % positive frequency
%     % get absolute values
%     coefs_abs_AWP = abs(coefsAWP{idx}).^2;
%     coefs_abs_WPT = abs(coefsWPT{idx}).^2;
%     
%     % remove zeros and NaN
%     coefs_abs_AWP(isnan(coefs_abs_AWP)) = [];
%     coefs_abs_WPT(isnan(coefs_abs_WPT)) = [];
%     coefs_abs_AWP(coefs_abs_AWP==0) = [];
%     coefs_abs_WPT(coefs_abs_WPT==0) = [];
%     
%     % calculate entropy of signal energy normed coefficients
%     batch_cost_awp(1,idx) = -sum((coefs_abs_AWP./signal_energy).*log(coefs_abs_AWP./signal_energy),'all');
%     batch_cost_real(1,idx) = -sum((coefs_abs_WPT./signal_energy).*log(coefs_abs_WPT./signal_energy),'all');
%     
% end
% % the costs of different batches are averaged
% cost_awp = mean(batch_cost_awp);
% cost_real = mean(batch_cost_real);
% 
% end

