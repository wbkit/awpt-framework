% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_2D_Reconstruction.m:
%       testing the decomposition and reconstruction of images using
%       the multi-dimensional AWPs
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           17.12.2019
%   last Revision:  26.02.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;


for test_case = 1:6
    switch test_case
        case 1
            try
                %% real wavelet test
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[3,3]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec)).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                if error_cameraman < 1e-3
                    fprintf('Reconstruction DWT: Passed\n');
                else
                    fprintf(2,'Reconstruction DWT: Failed\n');
                end
            catch
                fprintf(2,'Reconstruction DWT: Failed\n');
            end
            
        case 2
            try
                %% analytic wavelet test
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,3);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0,'hwlet',1,2,4,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0,'hwlet',1,2,4,'mid_phase');
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec)).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                if error_cameraman < 1e-3
                    fprintf('DT-CWT: Passed\n');
                else
                    fprintf(2,'DT-CWT: Failed\n');
                end
            catch
                fprintf(2,'DT-CWT: Failed\n');
            end
        case 3
            try
                %% full_tree mixed analytic wavelet test
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'full_tree',2,[3,3]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0,'hwlet',1,2,4,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec)).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                if error_cameraman < 1e-3
                    fprintf('AWPT in Dim 1, WPT in Dim 2: Passed\n');
                else
                    fprintf(2,'AWPT in Dim 1, WPT in Dim 2: Failed\n');
                end
            catch
                fprintf(2,'AWPT in Dim 1, WPT in Dim 2: Failed\n');
            end
        case 4
            warning('off','AWPT:Analysis:Samples');
            try
                %% full_tree uneven samples test
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image(1:15,1:120)./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'full_tree',2,[3,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',5,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec(1:15,1:120))).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                [~,warnId] = lastwarn;
                if error_cameraman < 1e-3
                    if isequal(warnId,'AWPT:Analysis:Samples')
                        fprintf('WPT (uneven samples): Passed\n');
                    else
                        fprintf(2,'WPT (uneven samples): Failed\n');
                    end
                else
                    fprintf(2,'WPT (uneven samples): Failed\n');
                end
            catch
                % catch warning of uneven samples, which were zeropadded
                fprintf('WPT (uneven samples): Failed\n');
            end
            warning('on','AWPT:Analysis:Samples');
        case 5
            try
                %% real preset_tree test
                clear;
                
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                coef_list = {[1,1;1,0],[2,3;1,1],...
                    [2,2;1,1],[1,0;1,1],...
                    [1,0;2,1],[2,0;2,0],...
                    [2,1;3,0],[2,1;3,1]};
                split_param = get_decomposition_structure(coef_list,'preset_tree');
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec)).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                if error_cameraman < 1e-3
                    fprintf('Custom WPT: Passed\n');
                else
                    fprintf(2,'Custom WPT: Failed\n');
                end
            catch
                fprintf(2,'Custom WPT: Failed\n');
            end
        case 6
            try
                %% real preset_tree test
                clear;
                
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                coef_list = {[1,1;1,0],[2,3;1,1],...
                    [2,2;1,1],[1,0;1,1],...
                    [1,0;2,1],[2,0;2,0],...
                    [2,1;3,0],[2,1;3,1],...
                    [2,3;0,0]};
                split_param = get_decomposition_structure(coef_list,'preset_tree');
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                % reconstruction
                Image_rec = AWPT_synthesis(coef,s_fil);
                
                error_cameraman = sum((abs(Image - Image_rec)).^2,'all')/...
                    sum((abs(Image)).^2,'all')*100;
                
                % define passed test
                if error_cameraman < 1e-3
                    fprintf('Custom WPT Frame: Passed\n');
                else
                    fprintf(2,'Custom WPT Frame: Failed\n');
                end
            catch
                fprintf(2,'Custom WPT Frame: Failed\n');
            end
    end
    
end




