% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_2D_AWP.m:  testing the decomposition and reconstruction of images using
%                  the multi-dimensional AWPs
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           03.12.2019
%   last Revision:  26.02.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear;
clc;


for test_case = 1:7
    switch test_case
        case 1
            %% wavelet_tree test
            try
                % test image
                TestImage = double(imread('cameraman.tif'));
                TestImage = TestImage./max(TestImage,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,2);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                plot_Subbands2D(coef,0.8);
                title('WPT of cameraman (DWT resolution)');
                fprintf('DWT: Passed\n')
            catch
                fprintf(2,'DWT: Failed\n')
            end
        case 2
            try
                %% preset_tree test
                % test image
                TestImage = double(imread('cameraman.tif'));
                TestImage = TestImage./max(TestImage,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,2);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0,'hwlet',1,3,4,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0,'hwlet',1,3,4,'mid_phase');
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                plot_Subbands2D(coef,0.8);
                title('AWP of cameraman (DWT resolution)');
                
                fprintf('DT-CWT: Passed\n')
            catch
                fprintf(2,'DT-CWT: Failed\n')
            end
        case 3
            try
                %% preset_tree test
                % test image
                TestImage = double(imread('cameraman.tif'));
                TestImage = TestImage./max(TestImage,[],'all');
                
                coef_list = {[1,1;1,0],[2,3;1,1],...
                    [2,2;1,1],[1,0;1,1],...
                    [1,0;2,1],[2,0;2,0],...
                    [2,1;3,0],[2,1;3,1]};
                % split parameter
                split_param = get_decomposition_structure(coef_list,'preset_tree');
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                plot_Subbands2D(coef,0.8);
                title('WPT of cameraman (custom resolution)');
                
                fprintf('Real-valued custom WPT: Passed\n')
            catch
                fprintf(2,'Real-valued WPT: Failed\n')
            end
        case 4
            try
                %% subband ordering test WPT
                % test image
                N = 256;
                n= 0:N-1;
                TestImage = 0;
                for k=0:8
                    TestImage = TestImage + exp(2i*pi*k*16/N*n).'* exp(2i*pi*k*16/N*n);
                end
                TestImage = TestImage + exp(-2i*pi*16/N*n).'*exp(2i*pi*96/N*n);
                
                % split parameter
                split_param = get_decomposition_structure([],'full_tree',2,4);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',10,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',10,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                
                plot_Subbands2D(coef,1);
                title('WPT of exp(2j*pi*f*t)');
                
                fprintf('WPT: Passed\n')
            catch
                fprintf(2,'WPT: Failed\n')
            end
        case 5
            try
                %% subband ordering test AWP
                % test image
                N = 256;
                n= 0:N-1;
                TestImage = 0;
                for k=0:8
                    TestImage = TestImage + exp(2i*pi*k*16/N*n).'* exp(2i*pi*k*16/N*n);
                end
                TestImage = TestImage + exp(-2i*pi*16/N*n).'*exp(2i*pi*96/N*n);
                
                % split parameter
                split_param = get_decomposition_structure([],'full_tree',2,4);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',10,1,0,'hwlet',1,3,4,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',10,1,0,'hwlet',1,3,4,'mid_phase');
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                
                plot_Subbands2D(coef,1);
                title('AWP of exp(2j*pi*f*t)');
                
                fprintf('AWPT: Passed\n')
            catch
                fprintf(2,'AWPT: Failed\n')
            end
        case 6
            try
                %% oscillation test WPT <-> AWP
                % test image
                N = 256;
                n= 0:N-1;
                TestImage = 0;
                for k=0:8
                    TestImage = TestImage + cos(2*pi*k*16/N*n)'* cos(2*pi*k*16/N*n);
                end
                
                % split parameter
                split_param = get_decomposition_structure([],'full_tree',2,4);
                
                %% WPT
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',10,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',10,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                
                plot_Subbands2D(coef,1);
                title('WPT of cosine excitation');
                %% AWP
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',10,1,0,'hwlet',1,3,4,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',10,1,0,'hwlet',1,3,4,'mid_phase');
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                
                plot_Subbands2D(coef,1);
                title('AWP of cosine excitation');
                
                fprintf('WPT of cosine excitation: Passed\n')
            catch
                fprintf(2,'WPT of cosine excitation: Failed\n')
            end
        case 7
            try
                %% preset_tree with conflicting coefficients test
                % test image
                TestImage = double(imread('cameraman.tif'));
                TestImage = TestImage./max(TestImage,[],'all');
                
                coef_list = {[1,1;1,0],[2,3;1,1],...
                    [2,2;1,1],[1,0;1,1],...
                    [1,0;2,1],[2,0;2,0],...
                    [2,1;3,0],[2,1;3,1],...
                    [2,3;0,0]};
                % split parameter
                split_param = get_decomposition_structure(coef_list,'preset_tree');
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(TestImage, a_fil,split_param);
                plot_Subbands2D(coef,0.8);
                title('WPT of cameraman (custom resolution)');
                fprintf('Real-valued custom decomposition structure with conflicting coefficients: Passed\n')
            catch
                fprintf(2,'Real-valued custom decomposition structure with conflicting coefficients: Failed\n')
            end
    end
    
end




