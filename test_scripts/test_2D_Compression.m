% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_2D_Compression.m:  
%       testing the decomposition, compression and reconstruction of images using
%       the multi-dimensional AWPs
%       The results are evaluated and compared against the DCT 
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           26.01.2020
%   last Revision:  26.02.2020
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clc;
figure;
            
for test_case = 1:4
    switch test_case
        case 1
            %% DCT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                [Nx, Ny] = size(Image);
                % transform
                I_dct = dct2(Image);
                
                % compression
                I_dct = reshape(I_dct,1,[]);
                N = length(I_dct);
                [~,sort_idx] = sort(abs(I_dct),'descend');
                [~,rev_sort_idx] = sort(sort_idx);
                
                I_dct = I_dct(sort_idx);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                SSE_dct = zeros(1,nK);
                PSNR_dct = zeros(1,nK);
                
                for i_K=1:nK
                    K=K_vec(i_K);
                    I_dct_comp = I_dct;
                    I_dct_comp(floor(K*N):end) = 0;
                    
                    I_dct_comp = I_dct_comp(rev_sort_idx);
                    I_dct_comp = reshape(I_dct_comp,Nx,[]);
                    
                    % reconstruction
                    Image_rec = idct2(I_dct_comp);
                    SSE_dct(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dct(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dct);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dct);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DCT Compression: Passed\n');
            catch
                fprintf(2,'DCT Compression: Failed\n');
            end
        case 2
            %% DWT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dwt = zeros(1,nK);
                SSE_dwt = zeros(1,nK);
                for i_K=1:nK
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,K);
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_dwt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dwt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dwt);hold on
                subplot(2,1,2);
                plot(K_vec,SSE_dwt);hold on
                fprintf('DWT Compression: Passed\n');
            catch
                fprintf(2,'DWT Compression: Failed\n');
            end
            
        case 3
            %% DT-CWT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'wavelet_tree',2,[4,4]);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0,'hwlet',1,1,1,'mid_phase');
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0,'hwlet',1,1,1,'mid_phase');
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_dtcwt = zeros(1,nK);
                SSE_dtcwt = zeros(1,nK);
                for i_K=1:nK
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,K,'abs');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_dtcwt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_dtcwt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_dtcwt);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_dtcwt);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('DT-CWT Compression: Passed\n');
            catch
                fprintf(2,'DT-CWT Compression: Failed\n');
            end
        case 4
            %% Signal-adapted WPT
            try
                clear;
                % test image
                Image = double(imread('cameraman.tif'));
                Image = Image./max(Image,[],'all');
                signal_energy = sum(Image.^2,'all');
                
                % split parameter
                split_param = get_decomposition_structure([],'full_frame',2,[4,4],@hCostEntropy,signal_energy,true);
                
                % filter design
                % dimension 1
                [a_fil{1},s_fil{1}] = get_filters('db',2,1,0);
                % dimension 2
                [a_fil{2},s_fil{2}] = get_filters('db',2,1,0);
                
                % decomposition
                [coef] = AWPT_analysis(Image, a_fil,split_param);
                [coef_list,~] = optimal_basis_search(coef);
                [split_param] = get_decomposition_structure(coef_list,'preset_tree');
                coef = AWPT_analysis(Image, a_fil, split_param);
                
                K_vec = 0.005:0.005:0.2;
                nK = length(K_vec);
                PSNR_sawpt = zeros(1,nK);
                SSE_sawpt = zeros(1,nK);
                for i_K=1:nK
                    K = K_vec(i_K);
                    % compression
                    coef_comp = hard_threshold_compression(coef,K,'abs');
                    
                    % reconstruction
                    Image_rec = AWPT_synthesis(coef_comp,s_fil);
                    
                    SSE_sawpt(i_K) = sum((abs(Image - Image_rec)).^2,'all')/...
                        sum((abs(Image)).^2,'all')*100;
                    PSNR_sawpt(i_K) = 10*log10(1/mean((abs(Image - Image_rec)).^2,'all'));
                end
                
                subplot(2,1,1);
                plot(K_vec,PSNR_sawpt);hold on
                ylabel('PSNR in dB');
                subplot(2,1,2);
                plot(K_vec,SSE_sawpt);hold on
                ylabel('SSE/E_0 in %')
                xlabel('Relative compression');
                fprintf('SA-WPT Compression: Passed\n');
            catch
                fprintf(2,'SA-WPT Compression: Failed\n');
            end
    end
end

subplot(2,1,1);
legend('DCT','DWT','DT-CWT','SA-WPT');
subplot(2,1,2);
legend('DCT','DWT','DT-CWT','SA-WPT');



function [cost_awp,cost_real] = hCostEntropy(coefsWPT,coefsAWP,signal_energy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hCostEntropy  handle for cost function of the coefficients
%               Entropy based
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate memory
% get number of batches
nBatches = length(coefsWPT);
batch_cost_awp = zeros(1,nBatches);
batch_cost_real = zeros(1,nBatches);

% do for every batch
for idx=1:nBatches
    
    % positive frequency
    % get absolute values
    coefs_abs_AWP = abs(coefsAWP{idx}).^2;
    coefs_abs_WPT = abs(coefsWPT{idx}).^2;
    
    % remove zeros and NaN
    coefs_abs_AWP(isnan(coefs_abs_AWP)) = [];
    coefs_abs_WPT(isnan(coefs_abs_WPT)) = [];
    coefs_abs_AWP(coefs_abs_AWP==0) = [];
    coefs_abs_WPT(coefs_abs_WPT==0) = [];
    
    % calculate entropy of signal energy normed coefficients
    batch_cost_awp(1,idx) = -sum((coefs_abs_AWP./signal_energy).*log(coefs_abs_AWP./signal_energy),'all');
    batch_cost_real(1,idx) = -sum((coefs_abs_WPT./signal_energy).*log(coefs_abs_WPT./signal_energy),'all');
    
end
% the costs of different batches are averaged
cost_awp = mean(batch_cost_awp);
cost_real = mean(batch_cost_real);


end

