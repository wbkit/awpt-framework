% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% test_FilterVisualization_2D.m:  
%       In this script, the two methods to visualize the base functions and
%       their spectrum in N dimensions are testet using the special case 2D. 
%       Every base function is completely
%       described using the stage, subband, and time step and 
%       most importantly the filter transfer functions.
%   
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           19.12.2019
%   last Revision:  26.02.2020
%
%   The two methods "calc_base_function_MultiDim.m" and 
%   "calc_base_response_MultiDim.m" are tested. 
%   In "calc_base_response_MultiDim" the wavelet filterbank is excited using
%   harmonic signals and the response of the specified coefficient is
%   measured. Therefore, this function ("calc_base_response_MultiDim") only allows 
%   a spectral information and spectral leakage is a problem. To reduce
%   spectral leakage use frequency bins, that fit into the time window N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prepare workspace 
clear;
close all;
clc;

% define base function
coef_index = [4,2;4,2];
pixel_index = [3,3];
Nx = 64;

%% test real wavelet packets
% filter design real wavelets
[a_fil{1},s_fil{1}] = get_filters('db',1,1,0);
[a_fil{2},s_fil{2}] = get_filters('db',1,1,0);
% calculate base functions in spectral domain
try
[base_WPT,base_WPT_FT,f_vec] = calc_base_function_ND(s_fil,coef_index,pixel_index,Nx);
    fprintf('WPT filter visualization with synthesis method: Passed\n');
catch
    fprintf(2,'WPT filter visualization with synthesis method: Failed\n');
end
% calc filter response
try
[coef_WPT,f_vec] = calc_base_response_ND(a_fil,coef_index,pixel_index,Nx,f_vec,1);
    fprintf('WPT filter visualization with spectral domain method: Passed\n');
catch
    fprintf(2,'WPT filter visualization with spectral domain method: Failed\n');
end

%% test analytical wavelet packets
% filter design
[a_fil{1},s_fil{1}] = get_filters('db',1,1,0,'hwlet',1,1,4, 'mid_phase');
[a_fil{2},s_fil{2}] = get_filters('db',1,1,0,'hwlet',1,1,4, 'mid_phase');
% calculate base functions in spectral domain
try
    [base_AWPT,base_AWPT_FT,f_vec] = calc_base_function_ND(s_fil,coef_index,pixel_index,Nx);
    fprintf('AWPT filter visualization with synthesis method: Passed\n');
catch
    fprintf(2,'AWPT filter visualization with synthesis method: Failed\n');
end
% calc filter response
try
    [coef_AWPT,f_vec] = calc_base_response_ND(a_fil,coef_index,pixel_index,Nx,f_vec,1);
    fprintf('AWPT filter visualization with spectral domain method: Passed\n');
catch
    fprintf(2,'AWPT filter visualization with spectral domain method: Failed\n');
end



%% create figure
% create mesh for 2D visualization in spectral domain
[Xf,Yf] = meshgrid(f_vec);
[Xt,Yt] = meshgrid(1:length(f_vec));
% DFT of basis function
hfig1_a = figure;
surf(Xf,Yf,abs(base_WPT_FT));
title('Real Wavelet Packets Transfer Function');
xlabel('f/kHz');
ylabel('f/kHz');
zlabel('Spectral density');
% basis function
hfig1_b = figure;
surf(Xt,Yt,real(base_WPT));
title('Real Wavelet Packets Base Function');
xlabel('x/Px');
ylabel('y/Px');
zlabel('Intensity');

% create figure
% DFT of basis function
hfig2_a = figure;
surf(Xf,Yf,abs(base_AWPT_FT));
title('Analytic Wavelet Packets Transfer Function');
xlabel('f/kHz');
ylabel('f/kHz');
zlabel('Spectral density');
% basis function
hfig2_b = figure;
surf(Xt,Yt,real(base_AWPT));
title('Analytic Wavelet Packets Base Function');
xlabel('x/Px');
ylabel('y/Px');
zlabel('Intensity');


