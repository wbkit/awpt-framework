% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function Np = calc_efillen(fil, P)
%% calc_efillen   calculates the effective length of a filter in iir structure
%                 G(z) = (b0*z^0 + b1*z^-1 + ...) / (a0*z^0 + a1*z^-1+ ...)
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 13.11.2019 (Matthias Baechle)
%
%   Input: fil   ...   cell array with analysis filters  
%                        {numerator,denominator}
%          P     ...   [double] energy threshold in percent
%
%   Output:
%          Np ...  [int] \in [1,N] with N = length of impulse response (=200) 
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract filter coefficients
b = fil{1};
a = fil{2};

% create impulse excitation signal
x = [1,zeros(1,199)];

% calculate impulse response
h = filter(b,a,x);

% calculate properties of impulse response
E = sum(h.^2);
E_int = cumsum(h.^2);

% calculate energy threshold and index, when threshold is exceeded
Ep = P/100*E;
indizes_all = find(E_int >= Ep);
if ~isempty(indizes_all)
    Np = indizes_all(1);
else
    % consider numerical artefacts
    Ep = ((P/100)-1e-10)*E;
    indizes_all = find(E_int >= Ep);
    if ~isempty(indizes_all)
        Np = indizes_all(1);
    else
        % if numerical artefact is unsolvable, use maximum length
        Np = 200;
    end
end
    
end





