% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil, s_fil] = calc_fb_effmid(a_fil,s_fil)
%% calc_fb_effmid   calculates the effective middle of all the filters 
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   last Revision: 18.12.2019 (Matthias Baechle)
%
%   Input: a_fil ...   cell array with analysis filters  
%                      {lowpass=1,bandpass=2}{real=1,imag=2}
%                        {coeffs_num=1,coeffs_denum=2,effectiveLength=3}
%          s_fil ...   cell array with synthesis filters  
%                      {lowpass=1,bandpass=2}{real=1,imag=2}
%                        {coeffs_num=1,coeffs_denum=2,effectiveLength=3}
%
%   Output:
%          a_fil ...  cell array with analysis filters 
%                     {lowpass=1,bandpass=2}{real=1,imag=2}
%                       {coeffs_num=1,coeffs_denum=2,effectiveLength=3,effectiveMiddle=4}
%          s_fil ...  cell array with synthesis filters
%                     {lowpass=1,bandpass=2}{real=1,imag=2}
%                       {coeffs_num=1,coeffs_denum=2,effectiveLength=3,effectiveMiddle=4}
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% lowpass
lp_re= a_fil{1}{1};
Eff_mid = calc_efimid(lp_re);
% use the same effective middle for real and imaginary filters
a_fil{1}{1}{4} = Eff_mid;
a_fil{1}{2}{4} = Eff_mid;
s_fil{1}{1}{4} = Eff_mid;
s_fil{1}{2}{4} = Eff_mid;

% bandpass real tree
hp_re= a_fil{2}{1};
Eff_mid = calc_efimid(hp_re);
% use the same effective middle for real and imaginary filters
a_fil{2}{1}{4} = Eff_mid;
a_fil{2}{2}{4} = Eff_mid;
s_fil{2}{1}{4} = Eff_mid;
s_fil{2}{2}{4} = Eff_mid;


end