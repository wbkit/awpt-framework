% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil,s_fil] = pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag)
%% pack2iir    packs the filter coefficients into cell array data structure
%
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 13.11.2019 (Matthias Baechle)
%
%   Input: 
%          afil_lp_real   ...   analysis real tree lowpass filter   (1 x nFilterCoefs)
%          afil_bp_real   ...   analysis real tree highpass filter  (1 x nFilterCoefs)
%          afil_lp_imag   ...   analysis imag tree lowpass filter   (1 x nFilterCoefs)
%          afil_bp_imag   ...   analysis imag tree highpass filter  (1 x nFilterCoefs)
%          sfil_lp_real   ...   synthesis real tree lowpass filter  (1 x nFilterCoefs)
%          sfil_bp_real   ...   synthesis real tree highpass filter (1 x nFilterCoefs)
%          sfil_lp_imag   ...   synthesis imag tree lowpass filter  (1 x nFilterCoefs)
%          sfil_bp_imag   ...   synthesis imag tree highpass filter (1 x nFilterCoefs)
%
%   Output:
%          afil   ...   (1 x 2) cell array of (1 x 2) cell array with
%                       analysis filters
%                       {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denum=2}
%          sfil   ...   (1 x 2) cell array of (1 x 2) cell array with
%                       synthesis filters
%                       {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denum=2}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% pack analysis filters
a_fil{1} = {{afil_lp_real,1},{afil_lp_imag,1}};
a_fil{2} = {{afil_bp_real,1},{afil_bp_imag,1}};
% pack synthesis filters
s_fil{1} = {{sfil_lp_real,1},{sfil_lp_imag,1}};
s_fil{2} = {{sfil_bp_real,1},{sfil_bp_imag,1}};

end