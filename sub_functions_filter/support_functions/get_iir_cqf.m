% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil,s_fil] = get_iir_cqf(afil_lp_real_num,afil_lp_real_denom)
%% get_iir_cqf   creates cell structure with the necessary filters 
%                for first stage IIR filters
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   Checked: 14.11.2019 (Matthias Baechle)
%
%   Input: afil_lp_real_num   ...   analysis lowpass filter numerator (1 x nFilterCoefs)
%          afil_lp_real_denom ...   analysis lowpass filter denominator (1 x nFilterCoefs) 
%
%   Output:
%          a_fil ...  cell array with analysis filters 
%                     {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denom=2}
%          s_fil ...  cell array with synthesis filters
%                     {lowpass=1,bandpass=2}{real=1,imag=2}{coeffs_num=1,coeffs_denom=2}
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% caclulate bandpass and synthese filters from analysis, lowpass filters

% calc imaginary tree filter from real tree filter 
afil_lp_imag_num = [afil_lp_real_num(end), afil_lp_real_num(1:end-1)];

% real tree
[afil_lp_real_num,afil_bp_real_num,sfil_lp_real_num,sfil_bp_real_num] = get_orth_cqf(afil_lp_real_num);
% imaginary tree
[afil_lp_imag_num,afil_bp_imag_num,sfil_lp_imag_num,sfil_bp_imag_num] = get_orth_cqf(afil_lp_imag_num);


%% caclulate denominators

afil_bp_real_denom = afil_lp_real_denom;
afil_lp_imag_denom = afil_lp_real_denom;
afil_bp_imag_denom =  afil_lp_real_denom;
sfil_lp_real_denom =  afil_lp_real_denom;
sfil_bp_real_denom =  afil_lp_real_denom;
sfil_lp_imag_denom =  afil_lp_real_denom;
sfil_bp_imag_denom =  afil_lp_real_denom;


%% pack the filters in cell array

% pack analysis filters
a_fil{1} = {{afil_lp_real_num,afil_lp_real_denom},{afil_lp_imag_num,afil_lp_imag_denom}};
a_fil{2} = {{afil_bp_real_num,afil_bp_real_denom},{afil_bp_imag_num,afil_bp_imag_denom}};

% synthesis filters
s_fil{1} = {{sfil_lp_real_num,sfil_lp_real_denom},{sfil_lp_imag_num,sfil_lp_imag_denom}};
s_fil{2} = {{sfil_bp_real_num,sfil_bp_real_denom},{sfil_bp_imag_num,sfil_bp_imag_denom}};

end

