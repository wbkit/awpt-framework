% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function m = calc_efimid(fil)
%% calc_efimid   calculates the effective middle of a filter in iir structure
%                 G(z) = (b0*z^0 + b1*z^-1 + ...) / (a0*z^0 + a1*z^-1+ ...)
%                
%   Author: Andreas Cnaus
%   Institution: Institute of Industrial Information Technology
%   Date: ??
%   last Revision: 18.12.2019 (Matthias Baechle)
%
%   Input: fil   ...   cell array with analysis filters  
%                        {numerator,denominator}
%
%   Output:
%          m ...  [int] \in [1,N] with N = length of impulse response (=200) 
%          
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% extract filter coefficients
b = fil{1};
a = fil{2};

% create impulse excitation signal
x = [1,zeros(1,199)];

% calculate impulse response
h = filter(b,a,x);

% calculate expectation value of time
t = 0:length(h)-1;
m = sum(t.*(h.^2)/sum(h.^2));

end