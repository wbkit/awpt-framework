% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil,s_fil] = get_filter_coefficients(fs_fil, fil)
%% get_filter_coefficients:  
%   calculates the analysis and synthesis filters 
%   from the parameters specified by fs_fil and fil
%
%   Author:         Andreas Cnaus
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           09.10.2019
%   last Revision:  12.12.2019
%
%   Input:  fs_fil    ...  struct with 4 fields with information on the
%                          first stage filter with name:
%                                   'db':   daubechies
%                                   'coif': Coiflets
%                                   'sym':  Symlets
%                                   'fk':   Fejer-Korovkin
%                                   'idabusfi': Intermediate Daubechies-Butterworth Scaling FIlter
%           fil       ...  struct with 5 fields with information on the
%                          filter for the stages > 1
%                          Possible names:
%                                   'hwlet'  : by selesnick
%                                   'bwlet'  : by selesnick
%                                   'qshift' : by kingsbury
%
%   Output: a_fil     ...  analysis filters
%                          {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                          {real=1,imag=2}
%                          {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
%           s_fil     ...  synthesis filters
%                          {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                          {real=1,imag=2}
%                          {coeffs_num=1,coeffs_denom=2}
%
% fs_fil.filterName - filter name
% fs_fil.nZeroes - number of zeros contributing to passband flatness
% fs_fil.nPoles - number of poles 
% fs_fil.nLength - first stage filter length
% fil.filterName - filter name
% fil.nLength - filter length
% fil.VanishingMoments - number of zeros at z=-1, vanishing moments
% fil.FractionalDelay - degree of fractional delay
% fil.FactorizationMode - factorization mode 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

useAnalyticWaveletPackets = ~isempty(fil.filterName);

if useAnalyticWaveletPackets
    [a_fil(1:2),s_fil(1:2)] = get_firstStageFilter(fs_fil);
    [a_fil(3:4),s_fil(3:4)] = get_secondStageFilter(fil);
else
    % if real wavelet packets use only real first stage filters
    [a_fil(1:2),s_fil(1:2)] = get_firstStageFilter(fs_fil);
    % set imaginary filter to real filter
    a_fil{1}{2} = a_fil{1}{1};
    a_fil{2}{2} = a_fil{2}{1};
    s_fil{1}{2} = s_fil{1}{1};
    s_fil{2}{2} = s_fil{2}{1};
    % set next stage filter to first stage filters
    a_fil(3:4) = a_fil(1:2);
    s_fil(3:4) = s_fil(1:2);
end

  
end

function [a_fil,s_fil] = get_firstStageFilter(fs_fil)
%  {first_lowpass=1,first_bandpass=2}
%  {real=1,imag=2}
%  {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
    switch fs_fil.filterName
        case 'idabusfi'
            % IIR
            
            % catch errors
            if fs_fil.nZeroes < 0
                fs_fil.nZeroes = 0;
                warning('Number of zeros has to be >= 0. Fallback to number of zeros = 0.');
            end
            if fs_fil.nPoles < 0
                fs_fil.nPoles = 0;
                warning('Number of poles has to be >= 0. Fallback to number of poles = 0.');
            end
            if mod(fs_fil.nPoles,2) ~= 0
                fs_fil.nPoles = fs_fil.nPoles-1;
                warning(['Number of poles has to be even, when using the Intermediate DAubechies-BUtterworth Scaling FIlter. Fallback to number of poles = ',num2str(fs_fil.nPoles),'.']);
            end
            
            % calculate filter
            [afil_lp_real_num,afil_lp_real_denom,~,~] = idabusfi(fs_fil.nZeroes,fs_fil.nPoles);
            afil_lp_real_num = afil_lp_real_num*sqrt(2);
            [a_fil,s_fil] = get_iir_cqf(afil_lp_real_num,afil_lp_real_denom);
        case 'apassfi'
            % IIR
            
            % catch errors
            if fs_fil.nPoles < 1
                fs_fil.nPoles = 1;
                warning('Number of poles has to be >= 1. Fallback to number of poles = 1.');
            end
            if mod(fs_fil.nPoles,2)==0
                fs_fil.nPoles = fs_fil.nPoles-1;
                warning(['Number of poles has to be odd, when using the AllPAss Sum Scaling FIlter. Fallback to number of poles = ',num2str(fs_fil.nPoles),'.']);
            end
            if fs_fil.nZeroes < 2*fs_fil.nPoles+1
                fs_fil.nZeroes = 2*fs_fil.nPoles+1;
                warning(['Number of zeros has to be >= 2*number of poles + 1. Fallback to number of zeros = ',num2str(fs_fil.nZeroes),'.']);
            end
            
            % calculate filters
            [~,~,afil_lp_real_num,afil_lp_real_denom] = apassfi(fs_fil.nZeroes,fs_fil.nPoles);
            [a_fil,s_fil] = get_iir_cqf(afil_lp_real_num,afil_lp_real_denom);
        otherwise
            % FIR: 'db', 'fk', 'coif', 'sym'
            wname = strcat(fs_fil.filterName,num2str(fs_fil.nLength));
            [~,~,afil_lp_real,~] = wfilters(wname);
            [a_fil,s_fil] = get_fir_cqf(afil_lp_real);
           
    end
    
    % effective filter length and effective middle
    [a_fil, s_fil] = calc_fb_efflen(a_fil,s_fil);
    [a_fil, s_fil] = calc_fb_effmid(a_fil,s_fil);

end

function [a_fil,s_fil] = get_secondStageFilter(fil)
%  {lowpass=3,bandpass=4}
%  {real=1,imag=2}
%  {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}

    switch fil.filterName
        case 'hwlet'
            % FIR
           [afil_lp_real,afil_lp_imag] = hwlet(fil.VanishingMoments,fil.FractionalDelay, fil.FactorizationMode);
           [afil_lp_real,afil_bp_real,sfil_lp_real,sfil_bp_real] = get_orth_cqf(afil_lp_real);
           [afil_lp_imag,afil_bp_imag,sfil_lp_imag,sfil_bp_imag] = get_orth_cqf(afil_lp_imag);
           [a_fil,s_fil]=pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag);
        case 'bwlet'
            % FIR
            [afil_lp_real,sfil_lp_real]=hwlet_bior(fil.VanishingMoments ,fil.VanishingMoments ,fil.FractionalDelay ,fil.FactorizationMode);
            [afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag]  = get_bior_filter(afil_lp_real,sfil_lp_real);
            [a_fil,s_fil]=pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag);
        case 'qshift'
            % FIR
           [afil_lp_real,afil_lp_imag] = qshiftgen(fil.nLength);
           sc = sqrt(2);
           afil_lp_real = sc*afil_lp_real.';
           afil_lp_imag = sc*afil_lp_imag.';
           [afil_lp_real,afil_bp_real,sfil_lp_real,sfil_bp_real] = get_orth_cqf(afil_lp_real);
           [afil_lp_imag,afil_bp_imag,sfil_lp_imag,sfil_bp_imag] = get_orth_cqf(afil_lp_imag);
           [a_fil,s_fil]=pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag);

        otherwise
            warning('ERROR, wavelet filter  is unknown. Fallback to hwlet filter.');
            [afil_lp_real,afil_lp_imag] = hwlet(1,4, 'mid_phase');
            [afil_lp_real,afil_bp_real,sfil_lp_real,sfil_bp_real] = get_orth_cqf(afil_lp_real);
            [afil_lp_imag,afil_bp_imag,sfil_lp_imag,sfil_bp_imag] = get_orth_cqf(afil_lp_imag);
            [a_fil,s_fil]=pack2iir(afil_lp_real,afil_bp_real,afil_lp_imag,afil_bp_imag,sfil_lp_real,sfil_bp_real,sfil_lp_imag,sfil_bp_imag);
    end
    
    % effective filter length and effective middle
    [a_fil, s_fil] = calc_fb_efflen(a_fil,s_fil);
    [a_fil, s_fil] = calc_fb_effmid(a_fil,s_fil);
end







