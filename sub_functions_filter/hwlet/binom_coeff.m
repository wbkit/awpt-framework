% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [binom_coeffs] = binom_coeff(n,k)
%% binom_coeff 
%       Calculates the binomial coefficients of n,k.
%       The input parameters may be vectors.
%       This function is based on the matlab function nchoosek(n,k)
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           24.02.2020
%   last Revision:  24.02.2020
%
%   Input:  n   ...     [1 x N] vector or [1 x 1] scalar
%           k   ...     [1 x N] vector or [1 x 1] scalar
%
%   Output: binom_coeffs    ...     [1 x N] vector
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set default value for ouput
binom_coeffs = 0;

%% condition input sizes
[n_n1,N] = size(n);
[n_k1,K] = size(k);

if n_n1 > N
    n = n.';
    N = n_n1;
end
if n_k1 > K
    k = k.';
    K = n_k1;
end

%% check the four cases 
%   1.) N==K 
%   2.) N==1 and K>1
%   3.) K==1 and N>1
%   4.) K==1 and N==1
if N == K
    binom_coeffs = zeros(1,N);
    for l=1:N
        binom_coeffs(l) = nchoosek(n(l),k(l));
    end
elseif (N==1) && (K>1)
    binom_coeffs = zeros(1,K);
    for l=1:K
        binom_coeffs(l) = nchoosek(n,k(l));
    end
elseif (N>1) && (K==1)
    binom_coeffs = zeros(1,N);
    for l=1:N
        binom_coeffs(l) = nchoosek(n(l),k);
    end
elseif (N==1) && (K==1)
    binom_coeffs = nchoosek(n,k);
else
    error('Wrong input dimension');
end



end




