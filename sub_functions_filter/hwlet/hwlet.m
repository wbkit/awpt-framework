% HWLET code by
% Ivan Selesnick, Polytechnic University, Brooklyn, NY
% selesi@taco.poly.edu
%
% and
% Markus Lang  <lang@dsp.rice.edu>
% Copyright: All software, documentation, and related files in this distribution
%            are Copyright (c) 1993  Rice University
%
% published in 
%   Ivan W. Selesnick, The design of approximate Hilbert transform pairs of wavelet bases,
%   IEEE Transaction on Signal Processing, 50(2): 1144-1152, 2002
%
% Permission is granted for use and non-profit distribution providing that this
% notice be clearly maintained. The right to distribute any portion for profit
% or as part of any commercial product is specifically reserved for the author.

function [h,g] = hwlet(K,L,phi)
% Hilbert transform pair of orthogonal wavelet bases
% h, g - scaling filters of length 2*(K+L)
% K - number of zeros at z=-1
% L - degree of fractional delay
% phi - factorization mode: mid_phase, min_phase  

n = 0:L-1;
t = 1/2;
d = cumprod([1, (L-n).*(L-n-t)./(n+1)./(n+1+t)]);
s1 = binom_coeff(2*K,0:2*K);
s2 = conv(d,d(end:-1:1));
s = conv(s1,s2);
M = K+L;
C = convmtx(s',2*M-1);
C = C(2:2:end,:);
b = zeros(2*M-1,1);
b(M) = 1;
r = (C\b)';
switch phi
    case 'min_phase'
        q = sfact(r);
    case 'mid_phase'
        q = sfactM(r);
    otherwise
        warning('ERROR, min and midphase factorization available. Fallback to mid_phase');
        q = sfactM(r);
end
f = conv(q,binom_coeff(K,0:K));
h = conv(f,d);
g = conv(f,d(end:-1:1));