% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [a_fil,s_fil] = get_filters(FirstStageFilter,fs_filterLength,nZeroes,nPoles,NextStageFilter,filterLength,VanishingMoments,FractionalDelay, factorizationMode)
%% get_filters set the filter parameters according to the 
%                        specified parameters and set default if nothing 
%                        is specified
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           10.10.2019
%   last Revision:  24.02.2020
%
%   Input:  FirstStageFilter    ... name of first stage filter [char array]
%                                   'db':   daubechies
%                                   'coif': Coiflets
%                                   'sym':  Symlets
%                                   'fk':   Fejer-Korovkin
%                                   'idabusfi': Intermediate Daubechies-Butterworth Scaling FIlter
%                                   'apassfi': AllPass Sum Scaling Filter
%           fs_filterLength     ... length of first stage filter if
%                                   nZeroes, nPoles unused
%           nZeroes             ... number of zeroes in z-transform [int]
%           nPoles              ... number of poles in z-transform [int]
%           NextStageFilter     ... name of next stage filter [char array]
%                                   'hwlet'  : by selesnick
%                                   'bwlet'  : by selesnick
%                                   'qshift' : by kingsbury
%           filterLength        ... length of filter if not defined by the
%                                   vanishing moments and fractional delay
%                                   [int >0]
%           VanishingMoments    ... [int >0] see mathematical definition
%           FractionalDelay     ... [int >0] see mathematical definition
%           factorizationMode   ... [char array] mode of factorization
%                                    'mid_phase', 'min_phase','lin_phase'
%
%   Output: a_fil     ...  analysis filters
%                          {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                          {real=1,imag=2}
%                          {coeffs_num=1,coeffs_denom=2,effective length=3, effective middle=4}
%           s_fil     ...  synthesis filters
%                          {first_lowpass=1,first_bandpass=2,lowpass=3,bandpass=4}
%                          {real=1,imag=2}
%                          {coeffs_num=1,coeffs_denom=2}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% set filter parameters
if nargin<9, fil.FactorizationMode = 'mid_phase';else,fil.FactorizationMode = factorizationMode;end
if nargin<8, fil.FractionalDelay = 5;else,fil.FractionalDelay = FractionalDelay;end
if nargin<7, fil.VanishingMoments = 1;else,fil.VanishingMoments = VanishingMoments;end
if nargin<6, fil.nLength = 10;else,fil.nLength = filterLength;end

% if only first stage filters are given, use real wavelet packets,
% described by empty fil.filterName
if nargin<5, fil.filterName = [];else,fil.filterName = NextStageFilter;end

if nargin<4, fs_fil.nPoles = 1;else,fs_fil.nPoles = nPoles;end
if nargin<3, fs_fil.nZeroes = 1;else,fs_fil.nZeroes = nZeroes;end
if nargin<2, fs_fil.nLength = 10;else,fs_fil.nLength = fs_filterLength;end
if nargin<1, fs_fil.filterName = 'db';else,fs_fil.filterName = FirstStageFilter;end

%% get filter coefficients
[a_fil,s_fil] = get_filter_coefficients(fs_fil, fil);

end