% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig_reconstructed = AWPT_synthesis(coefs, fil)
%% AWPT_synthesis
%       calculates the reconstruction of the signals from the coefficients
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           06.11.2019
%   last Revision:  26.02.2020
%
%   Input:  coefs    ...  (1 x nBatches) cell array of (nCoef x 5) cell
%                           array
%           fil       ...  (1x4) (cell array) filter coefficients (cell)
%                                 {First Lowpass {Real,Imag},...
%                                  First Bandpass {Real,Imag},...
%                                  Next Lowpass {Real,Imag},...
%                                  Next Bandpass {Real,Imag} }
%
%   Output: sig_reconstructed  ...  (1 x nBatches) signals (nSignales x N)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get coef structure
if iscell(coefs{1,1})
    nBatches = size(coefs,2);
    calcInBatches = true;
else
    nBatches = 1;
    coefs = {coefs};
    calcInBatches = false;
end

% check if filters for multiple dimensions are specified
filterMultiDim = iscell(fil{1}{1}{1});
if ~filterMultiDim, fil = {fil};end

% count number of specified filter
nDim = size(coefs{1}{1,2},1);
nFilters = length(fil);
if nFilters>nDim
    warning('AWPT:Synthesis:Filters','Too many filters specified. I am going to ignore the additional filters.');
    fil = fil(1:nDim);
elseif nFilters<nDim
    warning('AWPT:Synthesis:Filters','Not enough filters specified. I am going to replicate the last given filter to get the necessary number of filters.');
    fil = [fil, repmat(fil(nFilters),1,nDim-nFilters)];
end

% read parameters
coef_list = coefs{1}(:,2);

%% clean redundant coefficients
% preallocate memory
delete_index = [];

% check all list elements if they are already contained in a parent index
for n=1:length(coef_list)
    
    % check if coefficient has parent in the list
    has_ancestor = get_has_ancestor(coef_list{n,1},coef_list);
    
    % if coefficient has no parent, keep it
    if has_ancestor
        delete_index(end+1) = n;
    end
end

% delete found children indizes
for idx=1:nBatches
    coefs{idx}(:,[1,3]) = [];
    if ~isempty(delete_index)
        coefs{idx}(delete_index,:) = [];
    end
    coefs{idx}(:,[1,2,3]) = coefs{idx}(:,[2,1,3]);
end




%% Analytische WPT
sig_reconstructed = cell(1,nBatches);
for n=1:nBatches
    sig_reconstructed{n} = recursion_start_synthesis(coefs{n}, fil);
end

% unpack data if input was not packed
if ~calcInBatches
    sig_reconstructed = sig_reconstructed{1};
end

end


