% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [matrix] = plot_Subbands2D(coef,gamma)
%% plot_Subbands2D
%       Create a concatenated array with all subband combinations existing
%       and plot plot it using the imshow function with gamma correction
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           06.12.2019
%   last Revision:  25.02.2020
%
%   Input:  coef        ...     (nCoefs x 5) cell array [complex double]
%           gamma       ...     gamma correctur value default = 1
%                               
%   Output: matrix      ...    gives back the pixels of the picture
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set default values for plot representation
if nargin <2,gamma = 1;end

% get resulting matrix size
nSize = size(coef{1,1});
stages = coef{1,2}(:,1).';
if length(stages)==1, stages = [stages(1),0];end
sizeMatrix = nSize.*(2.^(stages));

% preallocate memory
matrix = zeros(sizeMatrix);

% place all coef_indizes inside the matrix
for n=1:size(coef,1)
    % get coef_index informations
    temp_matrix = coef{n,1};
    stages = coef{n,2}(:,1).';
    subbands = coef{n,2}(:,2).';
    % if data is one-dimensional-> add missing dimension as stage=0
    if length(stages)==1, stages = [stages(1),0];end
    if length(subbands)==1, subbands = [subbands(1),0];end
    
    % calc the position of the subband in the image
    delta = sizeMatrix./(2.^stages);
    PosStart = delta.*subbands+1;
    PosEnd = delta.*(subbands+1);
    
    matrix(fliplr((sizeMatrix(1)+1)-(PosStart(1):PosEnd(1))), PosStart(2):PosEnd(2) ) = temp_matrix;
    
end

% show resulting image
matrix_show = abs(matrix)/max(abs(matrix),[],'all');
figure;
imshow(matrix_show.^gamma,[0,1]);

end