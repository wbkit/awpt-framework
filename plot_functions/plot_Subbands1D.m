% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [matrix, f_m, t] = plot_Subbands1D(data, fs, show)
%plot_Subbands1D
%       plots the data in the transformed domain
%
%   Author:         Matthias Baechle and Daniel Schwaer
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           21.01.2019
%   last Revision:  26.02.2020 Matthias Baechle
%
%   Input:  data        ...     signal
%           fs          ...     sample frequency (needed for the Plot)
%           show        ...     [bool] show plot
%                               
%   Output: matrix      ...     gives back the pixels of the picture
%           f           ...     frequency vector
%           t           ...     time vector
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set default for variable argument
if nargin < 3
    show = true;
end

%% check input variables
% check index list
nIndizes = size(data,1);
if nIndizes==0, error('AWPT:Plot1D:noIndizes','Missing data in the transformed domain.'); end

% check if Batches were used
if iscell(data{1,1})
    warning('AWPT:Plot1D:Batches','Multiple batches were given. I am going to use the first batch and ignore the following.');
    data = data{1,1};
end

% check decomposition dimensions
if size(data{1,2},1) >1, error('AWPT:Plot1D:DecompositionDimension','Too many dimensions of the decomposition structure'); end

% check signal dimensions
if size(data{1,1},2) > 1
    warning('AWPT:Plot1D:SignalDimension','Too many dimensions of the signals. Only the first signal will be plotted');
end

% preallocate memory
data_withMeta = cell(0,5);

% cutoff redundant nodes
for n=1:nIndizes
    has_desc = get_has_descendant(data{n,2},data(:,2));
    if ~has_desc
        index = data{n,2};
        f_l = (fs/2)/(2^index(1))*index(2);
        f_u = (fs/2)/(2^index(1))*(index(2)+1);
        f_m = 1/2*(f_u+f_l);
        
        % save subbands with corresponding frequency edges
        data_withMeta(end+1,:) = {data{n,1}(:,1),f_m, f_l, f_u,index(1)};
    end
end

    
% extract unsorted frequency and coefficient vectors
f_m_unsorted   = [data_withMeta{:,2}];
f_l_unsorted   = [data_withMeta{:,3}];
f_u_unsorted   = [data_withMeta{:,4}];
level_unsorted = [data_withMeta{:,5}];
data_withMeta  = data_withMeta(:,1);

% sort elements
[f_m,sort_ind] = sort(f_m_unsorted);
f_l = f_l_unsorted(sort_ind);
f_u = f_u_unsorted(sort_ind);
level = level_unsorted(sort_ind);
data_withMeta = data_withMeta(sort_ind);

% find maximal length
coef_lengths = cellfun(@length, data_withMeta);
[maxLength,max_ind] = max(coef_lengths);

% create time vector
dt = 1/fs * 2^(level(max_ind));
t= 0:dt:((maxLength-1)*dt);

% preallocate memory
matrix = zeros(size(data_withMeta,1), maxLength);
matrix_plot = zeros(2*size(data_withMeta,1), maxLength);

%% data repeat
for m=1:length(data_withMeta)
    tempRep = repmat(data_withMeta{m}, 1, maxLength/coef_lengths(m));
    tempResh = reshape(tempRep.', [], maxLength);
    
    % create mesh matrix for output
    matrix(m, :) = tempResh;
    
    % create plot matrix with repeated subbands
    matrix_plot(2*m-1,:) = tempResh;
    matrix_plot(2*m,:) = tempResh;
    
end


%% optional surf plot using
if show
    % calc subband limits for plot
    f_u_plotLimits = f_u - 0.005*(f_u-f_l);
    f_l_plotLimits = f_l + 0.005*(f_u-f_l);
    f_plot = zeros(1,2*size(data_withMeta,1));
    f_plot(1:2:end) = f_l_plotLimits;
    f_plot(2:2:end) = f_u_plotLimits;
    figure;
    surf(t, f_plot, abs(matrix_plot), 'LineStyle', 'none')%, 'FaceColor', 'interp')
    view(0,90)
    xlabel('t');
    ylabel('f');
end


end