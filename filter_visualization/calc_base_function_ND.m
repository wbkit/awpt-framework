% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [base,base_FT,f] = calc_base_function_ND(s_fil,subband_index,position_index,Nx)
%% calc_base_function_ND
%       calculates the base function corresponding to the 
%       specified coefficient of the AWP
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           19.11.2019
%   last Revision:  20.02.2020
%
%   Input:  s_fil      ...  (1 x nDim)(1x4) (cell array) synthesis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           subband_index    ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           position_index  ...   (1 x nDim) [int] position of the
%                                 coefficient in the subband
%           Nx          ...  number of samples of base function
%
%   Output: base       ...  resulting (1 x N) of the wavelet synthesis
%                           filterbank
%           base_FT    ...  fft with fftshif of base (1 x N) complex
%           f          ...  -fs/2 .. fs/2 with df = fs/N
%                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% zeropadding to get power of 2 samples
Nx = 2^ceil(log2(Nx));

% read dimensionality
dim = size(subband_index,1);
if length(s_fil)~=dim || ~iscell(s_fil{1}{1}{1})
    base=0;
    base_FT=0;
    f=0;
    warning(['Wrong number of filters specified. Expected a cell array with ',num2str(dim),' filters'])
    return;
end

% calculate minimum allowed coefficient length
nCoeffs_min = zeros(1,dim);
for n=1:dim
    nCoeffs_min(n) = max([s_fil{n}{1}{1}{3},...
                          s_fil{n}{1}{2}{3},...
                          s_fil{n}{2}{1}{3},...
                          s_fil{n}{2}{2}{3}...
                          ]);
end

coef_Sizes = Nx./(2.^subband_index(:,1));
while(any((coef_Sizes*2) < nCoeffs_min))
    Nx = Nx *2 ;
    coef_Sizes = Nx./(2.^subband_index(:,1));
end

%% create coefficient structure
% create coefficient vector
coef_dummy = zeros(coef_Sizes.');
S.type = '()';
S.subs = num2cell(position_index);
coef_vec = subsasgn(coef_dummy,S,1);

% fill structure with coefficient of interest
coef = {[],subband_index,[],coef_vec,[]};

% create coef dummy meshgrid
subband_mesh = cell(1,dim);
subbands = 2.^subband_index(:,1)-1;
for n=1:dim
    subband_mesh{n} = 0:subbands(n);
end
[subband_mesh{:}] = ndgrid(subband_mesh{:});
subband_mesh = cellfun(@(x) reshape(x,1,[]), subband_mesh,'un',0);
subband_mesh = cell2mat(subband_mesh.');

% fill the rest with zeros
for k = 1:size(subband_mesh,2)
    if ~isequal(subband_index(:,2),subband_mesh(:,k))
        coef(end+1,:) = {[],[subband_index(:,1),subband_mesh(:,k)],[],coef_dummy,[]};
    end
end


%% reconstruct signal from coefficient structure
base  = AWPT_synthesis(coef, s_fil);


% distinguish analytical and real wavelet packets
for n=1:dim
    if ~isequal(s_fil{n}{1}{1},s_fil{n}{1}{2})
        base = myHilbert(0.5*base,n);
    end
end

% real wavelet packets
base_FT = conj(fftn(base));

% set meta data
df = 1/Nx;
f = (-Nx/2:Nx/2-1)*df;
base_FT = fftshift(base_FT);

end

function sig_c = myHilbert(sig,dim)
%% Hilbert Transform with configurable filtering dimension
%       transformation is performed in the Fourier domain
%
%   Input:  sig   ...  [M x N x ...]
%           dim   ...  in wich dimension to perform the transform
%   Ouput:
%           sig_c ...  Real(sig) + 1i*Imag(sig)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prepare parameters for variable dimensions assignment
N = size(sig,dim);
f = -N/2+1:N/2;

S.type='()';
S.subs= repmat({':'},1,ndims(sig));

% transform in the Fourier domain
sig_h = fft(sig,[],dim);
sig_h = fftshift(sig_h,dim);

% calculate filtering for f > 0
S.subs{dim} = f > 0;
sig_c_pos = subsref(sig_h,S);
sig_h = subsasgn(sig_h, S ,-1i*sig_c_pos);

% calculate filtering for f < 0
S.subs{dim} = f < 0;
sig_c_neg = subsref(sig_h,S);
sig_h = subsasgn(sig_h,S, 1i*sig_c_neg);

% remove mean
S.subs{dim} = (f==0);
sig_h = subsasgn(sig_h,S,0);

% perform inverse Fourier transform
sig_h = fftshift(sig_h,dim);
sig_c = sig +1i*ifft(sig_h,[],dim);

end