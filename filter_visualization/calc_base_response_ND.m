% Copyright (C) 2020  The AWP-Framework Authors 
% 
% This program is free software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function [coef,f_vec] = calc_base_response_ND(a_fil,subband_index,position_index,Nx,f_vec,fs)
%% calc_base_response_ND
%       calculates the response of AWP to a harmonics excitation
%       with specified frequencies
%
%   Author:         Matthias Baechle
%   Institution:    Institute of Industrial Information Technology (KIT)
%   Date:           09.12.2019
%   last Revision:  20.02.2020
%
%   Input:  a_fil       ...  (1 x nDim)(1x4) (cell array) analysis filter coefficients (cell)
%                               {First Lowpass {Real,Imag},...
%                                First Bandpass {Real,Imag},...
%                                Next Lowpass {Real,Imag},...
%                                Next Bandpass {Real,Imag} }
%           subband_index    ...  (nDim x 2)
%                                     dim 1   | stage  ,  subband|
%                                     dim 2   | stage  ,  subband|
%                                     dim 3   | stage  ,  subband|
%           position_index  ...   (1 x nDim) [int] position of the
%                                 coefficient in the subband
%           Nx         ...  number of samples of the harmonic excitation
%                           will be zeropadded to get a power of 2
%           f_vec      ...  (1 x K) vector of frequencies
%           fs         ...  sampling frequency
%
%   Output: coef      ...   resulting (K x K x ...) complex response of the wavelet packet
%                          
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% prepare excitation signal
% zeropadding to get power of 2 samples
Nx = 2^ceil(log2(Nx));

% variable arguments
if nargin < 7, fs = 1; end
if nargin < 6, f_vec = (-Nx/2:Nx/2-1)/Nx*fs; end

% read dimensionality
dim = size(subband_index,1);

% preallocate memory
f_mesh   = cell(1,dim);
t_mesh = cell(1,dim);

% prepare meshgrid inputs
for n=1:dim
    f_mesh{n} = f_vec;
    t_mesh{n} = (0:Nx-1)/fs;
end

% create meshgrids
[f_mesh{:}] = ndgrid(f_mesh{:});
[t_mesh{:}] = ndgrid(t_mesh{:});

% save size of f_mesh
fDims = size(f_mesh{1});

% create array with all subband combinations
f_mesh = cellfun(@(x) reshape(x,1,[]), f_mesh,'un',0);
f_mesh = cell2mat(f_mesh.');

% prepare wavelet decomposition
split_param = get_decomposition_structure({subband_index},'preset_tree');

%% sweep through the subband combinations

% result reference
Sref.type = '()';
Sasgn.type= '()';
Sref.subs = cell(1,length(position_index));
Sasgn.subs = cell(1,size(f_mesh,1));

for n=1:dim
    Sref.subs{n} = position_index(n);
end

% preallocate memory
coef = zeros(fDims);

for n=1:size(f_mesh,2)
    
    % create excitation signal
    argument = zeros(size(t_mesh{1}));
    for k=1:dim
        argument = argument + f_mesh(k,n)*t_mesh{k};
        Sasgn.subs{k} = find(f_mesh(k,n)==f_vec);
    end
    sig = exp(2i*pi*argument);

    % perform decomposition
    [coefs] = AWPT_analysis(sig,a_fil,split_param);
    
    % read response from the correct coefficient
    [~,index] = is_in_coef_list(subband_index,coefs(:,2));
    temp = subsref(coefs{index,1},Sref);
    coef = subsasgn(coef,Sasgn,temp);
    
end




end